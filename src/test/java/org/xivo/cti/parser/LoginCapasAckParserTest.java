package org.xivo.cti.parser;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.LoginCapasAck;
import org.xivo.cti.model.Capacities;
import org.xivo.cti.model.PhoneStatus;
import org.xivo.cti.model.Service;
import org.xivo.cti.model.UserStatus;
import org.xivo.cti.model.XiVOPreference;
import org.xivo.cti.model.Xlet;

public class LoginCapasAckParserTest {
    private LoginCapasAckParser loginCapasAckParser;

    @Before
    public void setUp() throws Exception {
        loginCapasAckParser  = new LoginCapasAckParser();
    }

    @Test
    public void parseLoginCapas() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"class\": \"login_capas\",\"presence\": \"available\", "
                        + "\"userid\": \"3\", \"ipbxid\": \"xivo\",\"appliname\": \"Client\","
                        + " \"timenow\": 1364373405.73,"
                        + "\"replyid\": 2, "
                        + " \"capas\": {"
                        + "\"regcommands\": {},"
                        + "\"ipbxcommands\": {},"
                        + "\"preferences\":  {\"xlet.identity.logagent\": \"1\", \"xlet.identity.pauseagent\": \"1\"},"
                        + " \"userstatus\": {"
                        + "\"available\": {\"color\": \"#08FD20\","
                        + " \"allowed\": [\"available\", \"away\",\"outtolunch\", \"donotdisturb\", \"berightback\"],"
                        + " \"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Disponible\"},"
                        + "\"disconnected\": {\"color\": \"#202020\", \"actions\": {\"agentlogoff\": \"\"}, \"longname\": \"D\u00e9connect\u00e9\"}, "
                        + "\"outtolunch\": {\"color\": \"#001AFF\", "
                        + "\"allowed\": [\"available\", \"away\", \"outtolunch\", \"donotdisturb\", \"berightback\"],"
                        + " \"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Parti Manger\"}"
                        + "},"
                        + "\"services\": [\"enablednd\", \"fwdrna\"], "
                        + "\"phonestatus\": {  "
                        + "\"16\": {\"color\": \"#F7FF05\", \"longname\": \"En Attente\"}, "
                        + "\"1\": {\"color\": \"#FF032D\", \"longname\": \"En ligne OU appelle\"}, "
                        + "\"0\": {\"color\": \"#0DFF25\", \"longname\": \"Disponible\"}, "
                        + "\"8\": {\"color\": \"#1B0AFF\", \"longname\": \"Sonne\"}"
                        + "},"
                        + "}, "
                        + "\"capaxlets\": ["
                        + "[\"identity\", \"grid\"], [\"search\", \"tab\"], [\"customerinfo\", \"tab\", \"1\"], "
                        + "[\"fax\", \"tab\", \"2\"], [\"dial\", \"grid\", \"2\"], [\"tabber\", \"grid\", \"3\"], "
                        + "[\"history\", \"tab\", \"3\"], [\"remotedirectory\", \"tab\", \"4\"], "
                        + "[\"features\", \"tab\", \"5\"], [\"mylocaldir\", \"tab\", \"6\"], [\"conference\", \"tab\", \"7\"]"
                        + "], " + "}");
        LoginCapasAck loginCapasAck = (LoginCapasAck) loginCapasAckParser.parse(jsonObject);
        assertNotNull("unable to decode login capas ack", loginCapasAck);
        assertEquals("unable to decode presence", loginCapasAck.presence, "available");
        assertEquals("unable to decode user id", loginCapasAck.userId, "3");
        assertEquals("unable to decode application name", loginCapasAck.applicationName, "Client");
        assertNotNull("unable to decode capacitied", loginCapasAck.capacities);
        assertNotNull("unable to decode xlets", loginCapasAck.xlets);

    }

    @Test
    public void parseLoginCapasAckNoPresence() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"class\": \"login_capas\", "
                        + "\"userid\": \"3\", \"ipbxid\": \"xivo\",\"appliname\": \"Client\","
                        + " \"timenow\": 1364373405.73,\"replyid\": 2, "
                        + " \"capas\": {"
                        + "\"regcommands\": {}, \"ipbxcommands\": {},"
                        + "\"preferences\":  {\"xlet.identity.logagent\": \"1\"},"
                        + " \"userstatus\": {\"available\": {\"color\": \"#08FD20\", \"longname\": \"Disponible\"},},"
                        + "\"services\": [\"enablednd\"], "
                        + "\"phonestatus\": {\"8\": {\"color\": \"#1B0AFF\", \"longname\": \"Sonne\"}},"
                        + "}, "
                        + "\"capaxlets\": [[\"identity\", \"grid\"]], "
                        + "}");
        LoginCapasAck loginCapasAck = (LoginCapasAck) loginCapasAckParser.parse(jsonObject);
        assertNotNull("unable to decode login capas ack", loginCapasAck);
        assertEquals("unable to decode presence", loginCapasAck.presence, "");

    }
    @Test
    public void parseXlets() throws JSONException {
        JSONArray xletsJson = new JSONArray(
                "[[\"identity\", \"grid\"], [\"search\", \"tab\"], [\"customerinfo\", \"tab\", \"1\"], "
                        + "[\"fax\", \"tab\", \"2\"], [\"dial\", \"grid\", \"2\"], [\"tabber\", \"grid\", \"3\"], "
                        + "[\"history\", \"tab\", \"3\"], [\"remotedirectory\", \"tab\", \"4\"], "
                        + "[\"features\", \"tab\", \"5\"], [\"mylocaldir\", \"tab\", \"6\"], [\"conference\", \"tab\", \"7\"]]");
        List<Xlet> xlets = loginCapasAckParser.parseXlets(xletsJson);

        assertNotNull("unable to decode xlets");

        Xlet xlet = new Xlet("fax", "tab", "2");

        assertThat(xlets, hasItem(xlet));
    }

    @Test
    public void parseCapacities() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"regcommands\": {}, "
                + "\"preferences\": {\"xlet.identity.logagent\": \"1\", \"xlet.identity.pauseagent\": \"1\"},"
                + " \"userstatus\": {" + "\"available\": {\"color\": \"#08FD20\","
                + " \"allowed\": [\"available\", \"away\",\"outtolunch\", \"donotdisturb\", \"berightback\"],"
                + " \"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Disponible\"},"
                + "\"disconnected\": {\"color\": \"#202020\", "
                + "\"actions\": {\"agentlogoff\": \"\"}, \"longname\": \"D\u00e9connect\u00e9\"}}, "
                + "\"services\": [\"enablednd\", \"fwdunc\"], "
                + "\"phonestatus\": {  \"16\": {\"color\": \"#F7FF05\", \"longname\": \"En Attente\"}, "
                + "\"8\": {\"color\": \"#1B0AFF\", \"longname\": \"Sonne\"}}, " + "\"ipbxcommands\": {}" + "}");

        Capacities capacities = loginCapasAckParser.parseCapacities(jsonObject);
        assertNotNull("unable to decode capacities", capacities);
        assertNotNull("unable to decode capacity preferences", capacities.getPreferences());
        assertNotNull("unable to decode userstatus", capacities.getUsersStatuses());
        assertNotNull("unable to decode services", capacities.getServices());
        assertNotNull("unable to decode phone statuses", capacities.getPhoneStatuses());

    }

    @Test
    public void parseCapacitiesNoPreferences() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"regcommands\": {}, " + "\"preferences\": false,"
                + " \"userstatus\": {" + "\"available\": {\"color\": \"#08FD20\","
                + " \"allowed\": [\"available\", \"away\",\"outtolunch\", \"donotdisturb\", \"berightback\"],"
                + " \"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Disponible\"},"
                + "\"disconnected\": {\"color\": \"#202020\", "
                + "\"actions\": {\"agentlogoff\": \"\"}, \"longname\": \"D\u00e9connect\u00e9\"}}, "
                + "\"services\": [\"enablednd\", \"fwdunc\"], "
                + "\"phonestatus\": {  \"16\": {\"color\": \"#F7FF05\", \"longname\": \"En Attente\"}, "
                + "\"8\": {\"color\": \"#1B0AFF\", \"longname\": \"Sonne\"}}, " + "\"ipbxcommands\": {}" + "}");

        Capacities capacities = loginCapasAckParser.parseCapacities(jsonObject);
        assertNotNull("unable to decode capacities", capacities);
        assertNotNull("unable to decode capacity preferences", capacities.getPreferences());
    }

    @Test
    public void parsePreferences() throws JSONException {
        JSONObject jsonPreferences = new JSONObject(
                "{\"xlet.identity.logagent\": \"1\", \"xlet.identity.pauseagent\": \"0\"}");

        List<XiVOPreference> preferences = loginCapasAckParser.parsePreferences(jsonPreferences);

        assertNotNull("unable to decode preferences", preferences);
        XiVOPreference xivoPreference = new XiVOPreference("xlet.identity.pauseagent", "0");
        assertThat("unable to decode xivo preferences", preferences, hasItem(xivoPreference));
    }

    @Test
    public void testParseUserStatuses() throws JSONException {
        JSONObject userStatusesJson = new JSONObject(
                "{"
                        + "\"available\": {\"color\": \"#08FD20\","
                        + " \"allowed\": [\"available\", \"away\",\"outtolunch\", \"donotdisturb\", \"berightback\"],"
                        + " \"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Disponible\"},"
                        + "\"outtolunch\": {\"color\": \"#001AFF\", \"allowed\": [\"available\", \"away\", \"outtolunch\", \"donotdisturb\", \"berightback\"],"
                        + " \"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Parti Manger\"}}");

        List<UserStatus> userStatuses = loginCapasAckParser.parseUserStatuses(userStatusesJson);
        assertEquals("all statuses not decoded", 2, userStatuses.size());
        assertEquals("status not decoded", "outtolunch", userStatuses.get(0).getName());
        assertEquals("status not decoded", "available", userStatuses.get(1).getName());

    }

    @Test
    public void testServices() throws JSONException {
        JSONArray servicesJson = new JSONArray("[\"enablednd\", \"fwdunc\"]");
        List<Service> services = loginCapasAckParser.parseServices(servicesJson);

        assertThat(services, hasItem(new Service("enablednd")));
        assertThat(services, hasItem(new Service("fwdunc")));
    }


    @Test
    public void testParsePhoneStatuses() throws JSONException {
        JSONObject phoneStatusesJson = new JSONObject(
                "{\"16\": {\"color\": \"#F7FF05\", \"longname\": \"En Attente\"}, "
                        + "\"1\": {\"color\": \"#FF032D\", \"longname\": \"En ligne OU appelle\"}, "
                        + "\"0\": {\"color\": \"#0DFF25\", \"longname\": \"Disponible\"}, "
                        + "\"8\": {\"color\": \"#1B0AFF\", \"longname\": \"Sonne\"}}, ");

        List<PhoneStatus> phoneStatuses = loginCapasAckParser.parsePhoneStatuses(phoneStatusesJson);
        assertNotNull("phone statuses not decoded", phoneStatuses);
        PhoneStatus phoneStatus = new PhoneStatus("1", "#FF032D", "En ligne OU appelle");
        assertThat(phoneStatuses, hasItem(phoneStatus));
    }


    @Test
    public void parseUserStatus() throws JSONException {
        JSONObject userStatusJson = new JSONObject("{\"color\": \"#08FD20\","
                + " \"allowed\": [\"available\", \"away\",\"outtolunch\", \"donotdisturb\", \"berightback\"], "
                + "\"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Disponible\"}");

        UserStatus userStatus = loginCapasAckParser.parseUserStatus("available", userStatusJson);
        assertNotNull("unable to decode user status", userStatus);
        assertEquals("invalid name", "available", userStatus.getName());
        assertEquals("unable to decode color", "#08FD20", userStatus.getColor());
        assertEquals("unable to decode longname", "Disponible", userStatus.getLongName());
        assertTrue("unable to decode allowed", userStatus.isAllowed("available"));
        assertEquals("unable to decode action name", "enablednd", userStatus.getActions().get(0).getName());
        assertEquals("unable to decode action parameter", "false", userStatus.getActions().get(0).getParameters());

    }

    @Test
    public void parseUserStatusNothingAllowed() throws JSONException {
        JSONObject userStatusJson = new JSONObject("{\"color\": \"#08FD20\","
                + "\"actions\": {\"enablednd\": \"false\"}, \"longname\": \"Disponible\"}");
        try {
            loginCapasAckParser.parseUserStatus("available", userStatusJson);
        } catch (JSONException e) {
            fail("allowed is optional");
        }

    }

    @Test
    public void parseUserStatusNoAction() throws JSONException {
        JSONObject userStatusJson = new JSONObject("{\"color\": \"#08FD20\", \"longname\": \"Disponible\"}");
        try {
            loginCapasAckParser.parseUserStatus("available", userStatusJson);
        } catch (JSONException e) {
            fail("action is optional");
        }
    }

}
