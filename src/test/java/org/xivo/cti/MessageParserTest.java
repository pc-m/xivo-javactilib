package org.xivo.cti;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.*;
import org.xivo.cti.message.request.UsersAdded;
import org.xivo.cti.model.*;
import org.xivo.cti.model.sheet.SheetInfo;

public class MessageParserTest {
    private MessageParser messageParser;

    @Before
    public void setUp() {
        messageParser = new MessageParser();
    }

    @Test
    public void parseStringMessage() throws InvalidParameterException, JSONException {
        String message = "{\"function\": \"listid\", " + "\"listname\": \"users\", " + "\"class\": \"getlist\","
                + "\"tipbxid\": \"xivo\", " + "\"list\": [\"11\", \"25\", \"12\", \"14\"], "
                + "\"timenow\": 1364919114.1 " + "}";
        UserIdsList usersList = (UserIdsList) messageParser.parseBuffer(message);
        assertNotNull("unable to decode users list", usersList);
    }

    @Test
    public void parseIds() throws JSONException {
        JSONObject jsonUsersList = new JSONObject("{\"function\": \"listid\", " + "\"listname\": \"users\", "
                + "\"class\": \"getlist\"," + "\"tipbxid\": \"xivo\", "
                + "\"list\": [\"11\", \"25\", \"12\", \"14\"], " + "\"timenow\": 1364919114.1 " + "}");
        UserIdsList usersList = (UserIdsList) messageParser.parse(jsonUsersList);
        assertNotNull("unable to decode Ids", usersList);
    }

    @Test
    public void parsePhoneConfigUpdate() throws JSONException {
        JSONObject phoneConfigUpdateJson = new JSONObject("{\"function\": \"updateconfig\", "
                + "\"class\": \"getlist\"," + "\"listname\": \"phones\"," + "\"tipbxid\": \"xivo\", "
                + "\"timenow\": 1365079540.57, " + "\"tid\": \"3\"," + "\"config\":{\"protocol\": \"sip\", "
                + "\"number\": \"1002\", \"iduserfeatures\": 34, " + "\"allowtransfer\": null, "
                + "\"context\": \"cho\", " + "\"initialized\": null, "
                + "\"rules_order\": 1, \"identity\": \"SIP/x2gjtw\"}" + "}");
        PhoneConfigUpdate phoneConfigUpdate = (PhoneConfigUpdate) messageParser.parse(phoneConfigUpdateJson);
        assertNotNull("unable to parse phone configuration update", phoneConfigUpdate);
        assertEquals("unable to parse phone id", Integer.valueOf(3), phoneConfigUpdate.getId());
        assertEquals("unable to parse user id", Integer.valueOf(34), phoneConfigUpdate.getUserId());
        assertEquals("unable to parse number", "1002", phoneConfigUpdate.getNumber());
        assertEquals("unable to parse context", "cho", phoneConfigUpdate.getContext());
    }

    @Test
    public void parsePhoneConfigUpdateNoConfiguration() throws JSONException {
        JSONObject phoneConfigUpdateJson = new JSONObject("{\"function\": \"updateconfig\", "
                + "\"class\": \"getlist\"," + "\"listname\": \"phones\"," + "\"tipbxid\": \"xivo\", "
                + "\"timenow\": 1365079540.57, " + "\"tid\": \"3\"," + "\"config\":{}" + "}");
        PhoneConfigUpdate phoneConfigUpdate = (PhoneConfigUpdate) messageParser.parse(phoneConfigUpdateJson);
        assertNotNull("unable to parse phone configuration update", phoneConfigUpdate);

    }

    @Test
    public void parseQueueConfigUpdate() throws JSONException {
        JSONObject queueConfigUpdateJson = new JSONObject("{\"function\": \"updateconfig\", "
                + "\"listname\": \"queues\", " + "\"tipbxid\": \"xivo\", " + "\"timenow\": 1382695066.94, "
                + "\"tid\": \"33\", " + "\"config\": {\"displayname\": \"blue ocean\", " + "\"name\": \"blue\", "
                + "\"context\": \"default\", " + "\"number\": \"3000\"}, " + "\"class\": \"getlist\"}");
        QueueConfigUpdate queueConfigUpdate = (QueueConfigUpdate) messageParser.parse(queueConfigUpdateJson);
        assertNotNull("unable to parse queue configuration update", queueConfigUpdate);
        assertEquals("unable to parse queue id", Integer.valueOf(33), queueConfigUpdate.getId());
        assertEquals("unable to parse queue name", "blue", queueConfigUpdate.getName());
        assertEquals("unable to parse queue displayname", "blue ocean", queueConfigUpdate.getDisplayName());
        assertEquals("unable to parse queue context", "default", queueConfigUpdate.getContext());
        assertEquals("unable to parse queue number", "3000", queueConfigUpdate.getNumber());
    }

    @Test
    public void parseAgentConfigUpdate() throws JSONException {
        JSONObject agentConfigUpdateJson = new JSONObject("{\"function\": \"updateconfig\", "
                + "\"listname\": \"agents\", " + "\"tipbxid\": \"xivo\", " + "\"timenow\": 1398414738.16, "
                + "\"tid\": \"11\", " + "\"config\": {" + "\"lastname\": \"Vaillant\", "
                + "\"firstname\": \"Michel\", " + "\"context\": \"sales\", " + "\"number\": \"2560\"}, "
                + "\"class\": \"getlist\"}");
        AgentConfigUpdate agentConfigUpdate = (AgentConfigUpdate) messageParser.parse(agentConfigUpdateJson);
        assertNotNull("unable to parse agent configuration update", agentConfigUpdate);
        assertEquals("unable to parse agent id", Integer.valueOf(11), agentConfigUpdate.getId());
        assertEquals("unable to parse agent last name", "Vaillant", agentConfigUpdate.getLastName());
        assertEquals("unable to parse agent first name", "Michel", agentConfigUpdate.getFirstName());
        assertEquals("unable to parse agent number", "2560", agentConfigUpdate.getNumber());
        assertEquals("unable to parse agent context", "sales", agentConfigUpdate.getContext());
    }

    @Test
    public void parseQueueMemberConfigUpdate() throws JSONException {
        JSONObject configUpdateJson = new JSONObject("{\"class\": \"getlist\", " + "\"config\": "
                + "{\"callstaken\": \"12\", " + "\"interface\": \"Agent/2501\", " + "\"lastcall\": \"19:00:58\", "
                + "\"membership\": \"static\", " + "\"paused\": \"1\", " + "\"penalty\": \"2\", "
                + "\"queue_name\": \"blue\", " + "\"status\": \"101\"}, " + "\"function\": \"updateconfig\", "
                + "\"listname\": \"queuemembers\", " + "\"tid\": \"Agent/2501,blue\", \"tipbxid\": \"xivo\"}");
        QueueMemberConfigUpdate queueMemberConfigUpdate = (QueueMemberConfigUpdate) messageParser
                .parse(configUpdateJson);
        assertNotNull("unable to parse queue member configuration update", queueMemberConfigUpdate);
        assertEquals("unable to parse queue member id", "Agent/2501,blue", queueMemberConfigUpdate.getId());
        assertEquals("unable to parse queue member agent number", "2501", queueMemberConfigUpdate.getAgentNumber());
        assertEquals("unable to parse queue member callstaken", 12, queueMemberConfigUpdate.getCallsTaken());
        assertEquals("unable to parse queue member lastcall", "19:00:58", queueMemberConfigUpdate.getLastCall());
        assertTrue("unable to parse queue member paused", queueMemberConfigUpdate.isPaused());
        assertEquals("unable to parse queue member penalty", 2, queueMemberConfigUpdate.getPenalty());
        assertEquals("unable to parse queue member queue name", "blue", queueMemberConfigUpdate.getQueueName());
        assertEquals("unable to parse queue member status", "101", queueMemberConfigUpdate.getStatus());
    }

    @Test
    public void parsePhoneStatusUpdate() throws JSONException {
        JSONObject jsonPhoneStatusUpdate = new JSONObject("{\"function\": \"updatestatus\", "
                + "\"listname\": \"phones\", " + "\"tipbxid\": \"xivo\", " + "\"timenow\": 1365090036.94, "
                + "\"status\": {\"channels\": [], \"queues\": [], \"hintstatus\": \"4\", \"groups\": []}, "
                + "\"tid\": \"2\", \"class\": \"getlist\"}");

        PhoneStatusUpdate phoneStatusUpdate = (PhoneStatusUpdate) messageParser.parse(jsonPhoneStatusUpdate);
        assertNotNull("unable de decode phone status update", phoneStatusUpdate);
        assertEquals("unable de decode id", Integer.valueOf(2), phoneStatusUpdate.getLineId());
        assertEquals("unable to decode hintstatus", "4", phoneStatusUpdate.getHintStatus());
    }

    @Test
    public void parsePhoneStatusUpdateNoChannels() throws JSONException {
        JSONObject jsonPhoneStatusUpdate = new JSONObject("{\"function\": \"updatestatus\", "
                + "\"listname\": \"phones\", " + "\"tipbxid\": \"xivo\", " + "\"timenow\": 1365090036.94, "
                + "\"status\": {\"hintstatus\": \"0\"}, " + "\"tid\": \"2\", \"class\": \"getlist\"}");

        PhoneStatusUpdate phoneStatusUpdate = (PhoneStatusUpdate) messageParser.parse(jsonPhoneStatusUpdate);
        assertNotNull("unable de decode phone status update", phoneStatusUpdate);
        assertEquals("unable de decode id", Integer.valueOf(2), phoneStatusUpdate.getLineId());
        assertEquals("unable to decode hintstatus", "0", phoneStatusUpdate.getHintStatus());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotParsePhoneStatusUpdateHinstatus0WithChannels() throws JSONException {
        JSONObject jsonPhoneStatusUpdate = new JSONObject(
                "{\"function\": \"updatestatus\", "
                        + "\"listname\": \"phones\", "
                        + "\"tipbxid\": \"xivo\", "
                        + "\"timenow\": 1365090036.94, "
                        + "\"status\": {\"channels\": [\"SIP/clneaq-00000274\"], \"queues\": [], \"hintstatus\": \"0\", \"groups\": []}, "
                        + "\"tid\": \"2\", \"class\": \"getlist\"}");
        messageParser.parse(jsonPhoneStatusUpdate);
    }

    @Test
    public void parseQueueStatusUpdate() throws JSONException {
        JSONObject statusUpdateMsg = new JSONObject("{\"function\": \"updatestatus\", " + "\"listname\": \"queues\", "
                + "\"tipbxid\": \"xivo\", " + "\"timenow\": 1382706345.71, "
                + "\"status\": {\"agentmembers\": [\"5\",\"12\",\"22\"], \"phonemembers\": [\"4\",\"17\"]}, "
                + "\"tid\": \"1\", " + "\"class\": \"getlist\"}");
        QueueStatusUpdate statusUpdate = (QueueStatusUpdate) messageParser.parse(statusUpdateMsg);
        assertNotNull("unable de decode queue status update", statusUpdate);
        assertThat("queue agent members not decoded", statusUpdate.getAgentMembers(), hasItem(Integer.valueOf(5)));
        assertThat("queue phone members not decoded", statusUpdate.getPhoneMembers(), hasItem(Integer.valueOf(17)));

    }

    @Test
    public void parseUserConfigUpdate() throws JSONException {
        JSONObject userConfigUpdateJson = new JSONObject(
                "{"
                        + "\"class\": \"getlist\","
                        + "\"function\": \"updateconfig\", "
                        + "\"listname\": \"users\", "
                        + "\"tipbxid\": \"xivo\","
                        + "\"timenow\": 1361440830.99,"
                        + "\"tid\": \"3\","
                        + "\"config\": {"
                        + "\"enablevoicemail\": 1,"
                        + "\"voicemailid\": 58,"
                        + "\"enablednd\": true, \"destrna\": \"4561\",\"enablerna\": 1, \"agentid\": 3,"
                        + "\"enableunc\": 1, \"destunc\": \"7890\",\"enablebusy\": True, \"destbusy\": \"54321\","
                        + "\"firstname\": \"Alice\", \"lastname\": \"Johnson\", \"fullname\": \"Alice Johnson\", \"mobilephonenumber\": \"0677889922\", "
                        + "\"linelist\": [\"5\",\"12\",\"22\"]}" + "}");

        UserConfigUpdate userConfigUpdate = (UserConfigUpdate) messageParser.parse(userConfigUpdateJson);
        assertNotNull("unable de decode user configuration update", userConfigUpdate);
        assertEquals("unable to decode user id ", 3, userConfigUpdate.getUserId());
        assertTrue("unable to decode dnd enabled", userConfigUpdate.isDndEnabled());
        assertTrue("unable to decode rna enabled", userConfigUpdate.isNaFwdEnabled());
        assertEquals("unable to decode rna destination", "4561", userConfigUpdate.getNaFwdDestination());
        assertEquals("unable to decode agentid", 3, userConfigUpdate.getAgentId());
        assertTrue("unable to decode unc enabled", userConfigUpdate.isUncFwdEnabled());
        assertEquals("unable to decode unc destination", "7890", userConfigUpdate.getUncFwdDestination());
        assertTrue("unable to decode busy enabled", userConfigUpdate.isBusyFwdEnabled());
        assertEquals("unable to decode busy destination", "54321", userConfigUpdate.getBusyFwdDestination());
        assertEquals("unable to decode first name", "Alice", userConfigUpdate.getFirstName());
        assertEquals("unable to decode last name", "Johnson", userConfigUpdate.getLastName());
        assertEquals("unable to decode full name", "Alice Johnson", userConfigUpdate.getFullName());
        assertEquals("unable to decode mobile phone number", "0677889922", userConfigUpdate.getMobileNumber());
        assertThat("line list not decoded", userConfigUpdate.getLineIds(), hasItem(Integer.valueOf(12)));
        assertTrue("unable to decode voicemail enabled", userConfigUpdate.isVoiceMailEnabled());
        assertEquals("unable to decode voicemail id ", 58, userConfigUpdate.getVoiceMailId());
    }

    @Test
    public void parseUserConfigUpdateNullIds() throws JSONException {
        JSONObject userConfigUpdateJson = new JSONObject(
                "{"
                        + "\"class\": \"getlist\","
                        + "\"function\": \"updateconfig\", "
                        + "\"listname\": \"users\", "
                        + "\"tipbxid\": \"xivo\","
                        + "\"timenow\": 1361440830.99,"
                        + "\"tid\": \"3\","
                        + "\"config\": {"
                        + "\"enablevoicemail\": 0,"
                        + "\"voicemailid\": null,"
                        + "\"enablednd\": true, \"destrna\": \"4561\",\"enablerna\": 1, \"agentid\": null,"
                        + "\"enableunc\": 1, \"destunc\": \"7890\",\"enablebusy\": True, \"destbusy\": \"54321\","
                        + "\"firstname\": \"Alice\", \"lastname\": \"Johnson\", \"fullname\": \"Alice Johnson\", \"mobilephonenumber\": \"0677889922\", "
                        + "\"linelist\": [\"5\",\"12\",\"22\"]}" + "}");

        UserConfigUpdate userConfigUpdate = (UserConfigUpdate) messageParser.parse(userConfigUpdateJson);
        assertEquals("unable to decode agentid", 0, userConfigUpdate.getAgentId());
        assertEquals("unable to decode voicemail id ", 0, userConfigUpdate.getVoiceMailId());
    }

    /*
     * {"class": "getlist", "function": "updatestatus", "listname": "agents",
     * "status": { "availability": "available", "availability_since":
     * 1369734094.63, "channel": null, "groups": [], "on_call_acd": false,
     * "on_call_nonacd": false, "on_wrapup": false, "phonenumber": "2002",
     * "queues": ["1"]}, "tid": 2, "timenow": 1369734094.63, "tipbxid": "xivo"}
     */
    @Test
    public void parseAgentStatusUpdate() throws JSONException {
        JSONObject jsonAgentStatusUpdate = new JSONObject(
                "{\"function\": \"updatestatus\", "
                        + "\"listname\": \"agents\", "
                        + "\"tipbxid\": \"xivo\", "
                        + "\"timenow\": 1365090036.94, "
                        + "\"status\": {\"availability\": \"available\", \"phonenumber\": \"2002\", \"on_wrapup\": false, \"on_call_acd\": false,"
                        + "\"on_call_nonacd\": true, \"groups\": [],\"queues\": [\"1\",\"11\",\"7\"]}, " + "\"tid\": \"2\", \"class\": \"getlist\"}");

        AgentStatusUpdate agentStatusUpdate = (AgentStatusUpdate) messageParser.parse(jsonAgentStatusUpdate);
        assertNotNull("unable de decode agent status update", agentStatusUpdate);
        assertEquals("unable de decode id", 2, agentStatusUpdate.getAgentId());
        assertEquals("unable to decode status.availability", Availability.AVAILABLE, agentStatusUpdate.getStatus()
                .getAvailability());
        assertEquals("unable to decode status.phonenumber", "2002", agentStatusUpdate.getStatus().getPhonenumber());
        assertEquals("unable to parse the reason", StatusReason.ON_CALL_NONACD, agentStatusUpdate.getStatus()
                .getReason());
    }

    /*
     * {"function": "updatestatus", "listname": "channels", "tipbxid": "xivo",
     * "timenow": 1378136055.76, "status": {"timestamp": 1378136055.65,
     * "holded": false, "commstatus": "ready", "parked": false, "state":
     * "Down"}, "tid": "SIP/barometrix_jyldev-00000000", "class": "getlist"}
     */

    @Test
    public void parseChannelStatusUpdate() throws JSONException {
        JSONObject jsonChannelStatusUpdate = new JSONObject(
                "{\"function\": \"updatestatus\", "
                        + "\"listname\": \"channels\", "
                        + "\"tipbxid\": \"xivo\", "
                        + "\"timenow\": 1378136055.76, "
                        + "\"status\": {\"timestamp\": 1378136055.65, \"holded\": false, \"commstatus\": \"ready\",\"parked\": false, \"state\": \"Down\"}, "
                        + "\"tid\": \"2\", \"class\": \"getlist\"}");

        ChannelStatus channelStatusUpdate = (ChannelStatus) messageParser.parse(jsonChannelStatusUpdate);

        assertNotNull("unable de decode channel status update", channelStatusUpdate);

    }

    @Test
    public void parseUserUpdateEmptyConfig() throws JSONException {
        JSONObject userConfigUpdateJson = new JSONObject("{" + "\"class\": \"getlist\","
                + "\"function\": \"updateconfig\", " + "\"listname\": \"users\", " + "\"tipbxid\": \"xivo\","
                + "\"timenow\": 1361440830.99," + "\"tid\": \"3\"," + "\"config\": {}" + "}");
        UserConfigUpdate userConfigUpdate = (UserConfigUpdate) messageParser.parse(userConfigUpdateJson);
        assertNotNull("unable de decode user configuration update", userConfigUpdate);
        assertEquals("unable to decode user id ", 3, userConfigUpdate.getUserId());
        assertFalse("unable to decode dnd enabled", userConfigUpdate.isDndEnabled());
    }

    @Test
    public void parseUnknowFunctionGetListMessage() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"capalist\": [2],\"class\": \"getlist\",\"replyid\": 1646064863,\"timenow\": 1361268824.68, \"function\": \"unkonwn\"}");
        assertNull("should silently ingnore not process getlist message", messageParser.parse(jsonObject));
    }

    @Test
    public void parseCallHistoryOutbound() throws JSONException {
        JSONObject callHistoryJson = new JSONObject("{\"class\": \"history\"," + "\"history\": ["
                + "{\"calldate\": \"2013-03-29T08:44:35.273998\", \"duration\": 0.148765, \"fullname\": \"*844201\"},"
                + "{\"calldate\": \"2013-03-28T16:56:48.071213\", \"duration\": 58.834744, \"fullname\": \"41400\"}],"
                + "\"mode\": 0, " + "\"replyid\": 529422441, " + "\"timenow\": 1364571477.33}");
        XiVOCall xivoCall = new XiVOCall("2013-03-28 16:56:48", 59, "41400", CallType.OUTBOUND);
        CallHistoryReply callHistoryReply = (CallHistoryReply) messageParser.parse(callHistoryJson);

        assertNotNull("cannot parse history", callHistoryReply);

        assertThat("cannot parse call history", callHistoryReply.getCallHistory(), hasItem(xivoCall));
    }

    @Test
    public void parseCallHistoryInbound() throws JSONException {
        JSONObject callHistoryJson = new JSONObject("{\"class\": \"history\"," + "\"history\": ["
                + "{\"calldate\": \"2013-03-29T08:44:35.273998\", \"duration\": 0.148765, \"fullname\": \"*844201\"},"
                + "{\"calldate\": \"2013-03-28T16:56:48.071213\", \"duration\": 58.834744, \"fullname\": \"41400\"}],"
                + "\"mode\": 1, " + "\"replyid\": 529422441, " + "\"timenow\": 1364571477.33}");
        XiVOCall xivoCall = new XiVOCall("2013-03-28 16:56:48", 59, "41400", CallType.INBOUND);
        CallHistoryReply callHistoryReply = (CallHistoryReply) messageParser.parse(callHistoryJson);

        assertNotNull("cannot parse history", callHistoryReply);

        assertThat("cannot parse call history", callHistoryReply.getCallHistory(), hasItem(xivoCall));
    }

    @Test
    public void parseCallHistoryMissed() throws JSONException {
        JSONObject callHistoryJson = new JSONObject("{\"class\": \"history\"," + "\"history\": ["
                + "{\"calldate\": \"2013-03-29T08:44:35.273998\", \"duration\": 0.148765, \"fullname\": \"*844201\"},"
                + "{\"calldate\": \"2013-03-28T16:56:48.071213\", \"duration\": 58.834744, \"fullname\": \"41400\"}],"
                + "\"mode\": 2, " + "\"replyid\": 529422441, " + "\"timenow\": 1364571477.33}");
        XiVOCall xivoCall = new XiVOCall("2013-03-28 16:56:48", 59, "41400", CallType.MISSED);
        CallHistoryReply callHistoryReply = (CallHistoryReply) messageParser.parse(callHistoryJson);

        assertNotNull("cannot parse history", callHistoryReply);

        assertThat("cannot parse call history", callHistoryReply.getCallHistory(), hasItem(xivoCall));
    }

    @Test
    public void paserUserSatusUpdate() throws JSONException {
        JSONObject userStatusUpdateJson = new JSONObject("{" + "\"class\": \"getlist\","
                + "\"function\": \"updatestatus\", " + "\"listname\": \"users\", " + "\"tipbxid\": \"xivo\","
                + "\"timenow\": 1361440830.99," + "\"tid\": \"7\"," + "\"status\": {\"availstate\": available}" + "}");

        UserStatusUpdate userStatusUpdate = (UserStatusUpdate) messageParser.parse(userStatusUpdateJson);

        assertNotNull("unable to decode user status update", userStatusUpdate);
        assertEquals("unable to decode user id ", 7, userStatusUpdate.getUserId());
        assertEquals("unable to decode user status", "available", userStatusUpdate.getStatus());
    }

    @Test
    public void parseLoginIdAck() throws JSONException {
        JSONObject jsonobject = new JSONObject("{\"class\": \"login_id\",\"sessionid\": \"21UaGDfst7\","
                + "\"timenow\": 1361268824.64,\"xivoversion\": \"1.2\"}");

        LoginIdAck loginIdAck = (LoginIdAck) messageParser.parse(jsonobject);

        assertNotNull("unable to decode login ack", loginIdAck);
        assertEquals("Can't sessionid", "21UaGDfst7", loginIdAck.sesssionId);
        assertEquals("Can't timenow", 1361268824.64, loginIdAck.timenow, 0.001);
        assertEquals("Can't xivoversion", "1.2", loginIdAck.xivoversion);

    }

    @Test
    public void parseLoginPasswordAck() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"capalist\": [2],\"class\": \"login_pass\","
                + "\"replyid\": 1646064863,\"timenow\": 1361268824.68}");

        LoginPassAck loginPassAck = (LoginPassAck) messageParser.parse(jsonObject);
        assertNotNull("unable to decode login ack", loginPassAck);
        assertEquals("Can't decode capalist", Integer.valueOf(2), loginPassAck.capalist.get(0));
        assertEquals("Can't decode timenow", 1361268824.68, loginPassAck.timenow, 0.001);
        assertEquals("Can't decode replyid", 1646064863, loginPassAck.replyId);
    }

    @Test
    public void parseLoginCapas() throws JSONException {
        JSONObject jsonObject = new JSONObject("{\"class\": \"login_capas\",\"presence\": \"available\", "
                + "\"userid\": \"3\", \"ipbxid\": \"xivo\",\"appliname\": \"Client\","
                + " \"timenow\": 1364373405.73,\"replyid\": 2, " + " \"capas\": {"
                + "\"regcommands\": {}, \"ipbxcommands\": {},"
                + "\"preferences\":  {\"xlet.identity.logagent\": \"1\"},"
                + " \"userstatus\": {\"available\": {\"color\": \"#08FD20\", \"longname\": \"Disponible\"},},"
                + "\"services\": [\"enablednd\"], "
                + "\"phonestatus\": {\"8\": {\"color\": \"#1B0AFF\", \"longname\": \"Sonne\"}}," + "}, "
                + "\"capaxlets\": [[\"identity\", \"grid\"]], " + "}");
        LoginCapasAck loginCapasAck = (LoginCapasAck) messageParser.parse(jsonObject);
        assertNotNull("unable to decode login capas ack", loginCapasAck);
        assertEquals("unable to decode presence", loginCapasAck.presence, "available");
        assertEquals("unable to decode user id", loginCapasAck.userId, "3");
        assertEquals("unable to decode application name", loginCapasAck.applicationName, "Client");
        assertNotNull("unable to decode capacitied", loginCapasAck.capacities);
        assertNotNull("unable to decode xlets", loginCapasAck.xlets);

    }

    @Test(expected = IllegalArgumentException.class)
    public void parseUnknowMessage() throws JSONException {
        JSONObject jsonObject = new JSONObject(
                "{\"capalist\": [2],\"class\": \"unexisting_message_class\",\"replyid\": 1646064863,\"timenow\": 1361268824.68}");

        messageParser.parse(jsonObject);

    }

    @Test(expected = IllegalArgumentException.class)
    public void parseNullMessage() throws JSONException {
        messageParser.parse(null);
    }

    @Test
    public void parseSheet() throws JSONException {
        String payload = "AAADjnicndPRToMwFAbgV6m9n8A0ZheFZeqFejGX6N2ymA4O40RosS0b+EY+hy8mkyB1G3OxV01z/i/nnKRsXGYpWYPSKIVPvXOXEhChjF"
                + "CsfFqYeDCi44DlSsaYQsAKDSpgKAwowVMieAY+xXxZYkQDdja/uZ08T+YlruViETCnLdyLbBJQYCUi5OnxRJhwISC1Mk/3M8fE+gouB25z"
                + "RseJWIaFtgDxR5PvmFvV3k6xTgDMy5spkEgVgfKp63r1+prs9p0SU+X1fSqFPeu306VbCUUsD0lTmbWQQZPa0jV5sLAtcBi7+MEeFa6w7q"
                + "YFoTSW1wx3Ejns+iuyzw8le8hhPco+WGmjePWfke/45hUqIDMEFUIjW1qf3buBpYyqng2cwPZuYYfttvALdZrv5LTf6wuGtjLE";
        /*
         * Readable form of the tesst xml payload: <?xml version="1.0"
         * encoding="utf-8"?> <profile> <user> <internal
         * name="ipbxid"><![CDATA[xivo]]></internal> <internal
         * name="where"><![CDATA[dial]]></internal> <internal
         * name="channel"><![CDATA[SIP/tfs6e4-00000008]]></internal> <internal
         * name="focus"><![CDATA[no]]></internal> <internal
         * name="zip"><![CDATA[1]]></internal> <sheet_qtui order="0010"
         * name="qtui" type="None"><![CDATA[]]></sheet_qtui> <sheet_info
         * order="0010" name="Nom" type="title"><![CDATA[B J]]></sheet_info>
         * <sheet_info order="0030" name="Origine"
         * type="text"><![CDATA[intern]]></sheet_info> <sheet_info order="0020"
         * name="Numéro" type="text"><![CDATA[2001]]></sheet_info> <systray_info
         * order="0010" name="Nom" type="title"><![CDATA[Hawkeye
         * Pierce]]></systray_info> <systray_info order="0030" name="Origine"
         * type="body"><![CDATA[intern]]></systray_info> <systray_info
         * order="0020" name="Numéro"
         * type="body"><![CDATA[2001]]></systray_info> </user> </profile>
         * ----------
         */
        JSONObject sheetJson = new JSONObject("{\"timenow\": 1361444639," + "\"class\": \"sheet\","
                + "\"compressed\": true," + "\"serial\": \"xml\"," + "\"payload\": \"" + payload + "\","
                + "\"channel\": \"SIP/e6fhff-00000007\"}");

        Sheet sheet = (Sheet) messageParser.parse(sheetJson);
        assertNotNull("unable to parse sheet message", sheet);
        assertEquals("unable to parse sheet date parameter", 1361444639, sheet.timenow.getTime());
        assertEquals("unable to parse sheet 'compressed' parameter", true, sheet.compressed);
        assertEquals("unable to parse sheet channel parameter", "SIP/e6fhff-00000007", sheet.channel);
        assertNotNull("unable to parse sheet payload", sheet.payload);
        ArrayList<SheetInfo> sheetInfo = (ArrayList<SheetInfo>) sheet.payload.getProfile().getUser().getSheetInfo();

        int sheetInfoListSize = sheetInfo.size();
        assertEquals("unable to parse payload xml content, wrong number of sheetInfo elements ", 3, sheetInfoListSize);
        Iterator<SheetInfo> iterator = sheetInfo.iterator();
        while (iterator.hasNext()) {
            SheetInfo infoItem = iterator.next();
            if (infoItem.getName().equals("Numéro")) {
                assertEquals("unable to parse sheet payload, wrong number", "2001", infoItem.getValue());
                assertEquals("unable to parse sheet payload, wrong numero/order", 20, infoItem.getOrder().intValue());
                assertEquals("unable to parse sheet payload, wrong numero/type", "text", infoItem.getType());

            }
            if (infoItem.getName().equals("Origine")) {
                assertEquals("unable to parse sheet payload, wrong origine", "intern", infoItem.getValue());
                assertEquals("unable to parse sheet payload, wrong origine/order", 30, infoItem.getOrder().intValue());
                assertEquals("unable to parse sheet payload, wrong origine/type", "text", infoItem.getType());
            }
        }
    }

    @Test
    public void parseSheetIgnorePayload() throws JSONException {
        messageParser.ignoreSheetPlayload();

        JSONObject sheetJson = new JSONObject("{\"timenow\": 1361444639," + "\"class\": \"sheet\","
                + "\"compressed\": true," + "\"serial\": \"xml\","
                + "\"payload\": \"AAADjnicndPRToMwFAbgV6m9n8A0ZheFZeqFejGX.......\","
                + "\"channel\": \"SIP/e6fhff-00000007\"}");

        Sheet sheet = (Sheet) messageParser.parse(sheetJson);

        assertNotNull("unable to parse sheet message", sheet);

    }

    @Test
    public void parseDirectoryResult() throws JSONException {
        JSONObject jsonDirectoryResult = new JSONObject("{\"status\": \"ok\", " + "\"resultlist\": [], "
                + "\"timenow\": 1378813893.18, "
                + "\"headers\": [\"Nom\", \"Num\u00e9ro\", \"Entreprise\", \"E-mail\", \"Source\"], "
                + "\"replyid\": 4, " + "\"class\": \"directory\"}");

        DirectoryResult directoryResult = (DirectoryResult) messageParser.parse(jsonDirectoryResult);

        assertNotNull("unable to parse directory result", directoryResult);
    }

    @Test
    public void parseQueueStats() throws JSONException {
        JSONObject queueStat = new JSONObject(
                "{\"stats\": {\"1\": {\"Xivo-TalkingAgents\": \"0\", \"Xivo-AvailableAgents\": \"1\", \"Xivo-EWT\": \"6\"}}, \"class\": \"getqueuesstats\", \"timenow\": 1384512350.25}");
        QueueStatistics queueStatistics = (QueueStatistics) messageParser.parse(queueStat);
        assertNotNull("unable to parse queue statistics", queueStatistics);
    }

    @Test
    public void parseIpbxCommandResponse() throws JSONException {
        JSONObject ipbxCmdResponse = new JSONObject(
                "{\"class\": \"ipbxcommand\", \"error_string\": \"agent_login_invalid_exten\", \"timenow\": 1400680396.41}");
        IpbxCommandResponse ipbxCommandResponse = (IpbxCommandResponse) messageParser.parse(ipbxCmdResponse);
        assertNotNull("unable to parse ipbx command response", ipbxCommandResponse);
        assertEquals("unable to parse ipbx command response error string", "agent_login_invalid_exten",
                ipbxCommandResponse.getError_string());
    }

    /*
     * {"class": "getlist", "function": "delconfig","list": ["Agent/2602,pool"],
     * "listname": "queuemembers", "timenow": 1403577693.24, "tipbxid": "xivo"}
     */

    @Test
    public void parseDelConfigQueueMember() throws JSONException {
        JSONObject queueMemberDelConfig = new JSONObject("{\"class\": \"getlist\", " + "\"function\": \"delconfig\","
                + "\"list\": [\"Agent/2602,pool\"], "
                + "\"listname\": \"queuemembers\", \"timenow\": 1403577693.24, \"tipbxid\": \"xivo\"}");
        QueueMemberRemoved queueMemberRemoved = (QueueMemberRemoved) messageParser.parse(queueMemberDelConfig);

        assertNotNull("Unable to parse queue member removed", queueMemberRemoved);
        assertEquals("Unable to parse agent number","2602",queueMemberRemoved.getAgentNumber());
        assertEquals("Unable to parse queue name","pool",queueMemberRemoved.getQueueName());

    }
    // {"function": "addconfig", "listname": "users", "tipbxid": "xivo", "list": ["99"], "timenow": 1417188396.2, "class": "getlist"}

    @Test
    public void parseAddUser() throws JSONException {
        JSONObject userAddedMsg = new JSONObject("{\"function\": \"addconfig\", \"listname\": \"users\", " +
                "\"tipbxid\": \"xivo\", \"list\": [\"99\",\"54\"], \"timenow\": 1417188396.2, \"class\": \"getlist\"}");
        UsersAdded usersAdded = (UsersAdded) messageParser.parse(userAddedMsg);

        assertNotNull("Unable to parse user added", usersAdded);
    }

    @Test
    public void parseAddQueueMember() throws JSONException {
        JSONObject queueMemberAddedMsg = new JSONObject("{\"function\": \"addconfig\", \"listname\": \"queuemembers\", " +
                "\"tipbxid\": \"xivo\", \"list\": [\"Agent/75000,blue\"], \"timenow\": 1417188396.2, \"class\": \"getlist\"}");
        QueueMemberAdded queueMemberAdded = (QueueMemberAdded) messageParser.parse(queueMemberAddedMsg);

        assertNotNull("Unable to parse queue member added", queueMemberAdded);
        assertEquals("Unable to parse agent number","75000",queueMemberAdded.getAgentNumber());
        assertEquals("Unable to parse queue name", "blue", queueMemberAdded.getQueueName());
    }

    @Test
    public void parseMeetmeUpdate() throws JSONException {
        JSONObject meetmeUpdateMsg = new JSONObject("{\"class\": \"meetme_update\", \"config\": {" +
                "\"44998\": {\"member_count\": 1," +
                             "\"members\": {\"1\": {\"join_order\": 1, \"join_time\": 1423660090.624006, \"muted\": \"No\", \"name\": \"John Doe\", \"number\": \"44203\"}}," +
                             "\"name\": \"test\"," +
                             "\"number\": \"44998\"," +
                             "\"pin_required\": \"Yes\"," +
                             "\"start_time\": 1423660090.624022}" +
                "}, \"timenow\": 1423660090.627437}");
        MeetmeUpdate meetmeUpdate = (MeetmeUpdate) messageParser.parse(meetmeUpdateMsg);

        assertNotNull("Unable to parse meetme update", meetmeUpdate);
        List<Meetme> meetmes = meetmeUpdate.getMeetmeList();
        assertNotNull("Unable to parse meetme member list", meetmes);
        assertEquals("Expected 1 meetme", 1, meetmes.size());

        Meetme meetme = meetmes.get(0);
        assertEquals("Could not parse meetme number", "44998", meetme.getNumber());
        assertEquals("Could not parse meetme name", "test", meetme.getName());
        assertTrue("Could not parse pinRequired", meetme.isPinRequired());
        assertEquals("Could not parse start time", new Date(1423660090624L), meetme.getStartTime());
        assertEquals("Expected 1 meetme member", 1, meetme.getMembers().size());

        MeetmeMember member = meetme.getMembers().get(0);
        assertEquals("Could not parse join order", 1, member.getJoinOrder());
        assertEquals("Could not parse join time", new Date(1423660090624L), member.getJoinTime());
        assertFalse("Could not parse muted", member.isMuted());
        assertEquals("Could not parse name", "John Doe", member.getName());
        assertEquals("Could not parse number", "44203", member.getNumber());
    }
    /*
        {"function": "updatestatus", "listname": "voicemails", "tipbxid": "xivo", "timenow": 1424898289.3, "status": {"new": "0", "waiting": null, "old": "1"}, "tid": "58", "class": "getlist"}
     */
    @Test
    public void parseVoiceMailStatus() throws JSONException {
        JSONObject voiceMailStatusMsg = new JSONObject("{\"function\": \"updatestatus\", \"listname\": \"voicemails\", " +
                "\"tipbxid\": \"xivo\", \"timenow\": 1424898289.3, " +
                "\"status\": {\"new\": \"22\", \"waiting\": \"33\", \"old\": \"58\"}, \"tid\": \"58\", \"class\": \"getlist\"}");

        VoiceMailStatusUpdate voiceMailStatusUpdate = (VoiceMailStatusUpdate)messageParser.parse(voiceMailStatusMsg);

        assertNotNull("Unable to parse voicemail status update", voiceMailStatusUpdate);
        assertEquals("Unable to parse voice mail id", 58, voiceMailStatusUpdate.getVoiceMailId());
        assertEquals("unable to parse new messages",22,voiceMailStatusUpdate.getNewMessages());
        assertEquals("unable to parse waiting messages",33,voiceMailStatusUpdate.getWaitingMessages());
        assertEquals("unable to parse old messages",58,voiceMailStatusUpdate.getOldMessages());

    }
}