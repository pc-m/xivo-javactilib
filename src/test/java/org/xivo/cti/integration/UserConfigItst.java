package org.xivo.cti.integration;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.MessageDispatcher;
import org.xivo.cti.listener.ConfigListener;
import org.xivo.cti.listener.IdsListener;
import org.xivo.cti.message.*;

public class UserConfigItst extends CtiItst implements ConfigListener, IdsListener {

    @Before
    public void setUp() throws Exception {
        messageDispatcher = new MessageDispatcher();
        messageDispatcher.addListener(LoginCapasAck.class, this);
        messageDispatcher.addListener(LoginIdAck.class, this);
        messageDispatcher.addListener(LoginPassAck.class, this);
        messageDispatcher.addListener(UserIdsList.class, this);
        messageDispatcher.addListener(PhoneIdsList.class, this);
        messageDispatcher.addListener(UserStatusUpdate.class, this);
        messageDispatcher.addListener(UserConfigUpdate.class, this);
    }

    @Test
    public void requestContactCenterConfig() throws IOException, JSONException, TimeoutException {
        connectToServer();
        loginToCtiServer();

        getVoiceMailStatus();
        consumeMessages(1);

        getPhones();

        CtiResponseMessage<?> ctiMessage = waitForMessage("");
        messageDispatcher.dispatch(ctiMessage);

        getUsersList();

        ctiMessage = waitForMessage("");
        messageDispatcher.dispatch(ctiMessage);

        consumeMessages(5);
        disconnect();
    }

    private void getVoiceMailStatus() {
        JSONObject query = messageFactory.createGetVoicemailStatus("58");
        sendMessage(query);
    }
    private void getPhones() {
        JSONObject query = messageFactory.createGetPhonesList();
        sendMessage(query);
    }

    private void getUsersList() {
        JSONObject query = messageFactory.createGetUsersList();
        sendMessage(query);
    }

    @Override
    public void onUserConfigUpdate(UserConfigUpdate userConfigUpdate) {
        System.out.println("User config updated " + userConfigUpdate.getUserId());

    }

    @Override
    public void onUserStatusUpdate(UserStatusUpdate userStatusUpdate) {
        System.out.println("User " + userStatusUpdate.getUserId() + " status update " + userStatusUpdate.getStatus());

    }

    @Override
    public void onPhoneConfigUpdate(PhoneConfigUpdate phoneConfigUpdate) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onPhoneStatusUpdate(PhoneStatusUpdate phoneStatusUpdate) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onUserIdsLoaded(UserIdsList userIdsList) {
        for (Integer userId : userIdsList.getIds()) {
            System.out.println("Requesting get user for User id : " + userId);
            JSONObject getUserMessage = messageFactory.createGetUserConfig(userId.toString());
            sendMessage(getUserMessage);
        }
        consumeMessages(userIdsList.getIds().size());
    }

    @Override
    public void onPhoneIdsLoaded(PhoneIdsList phoneIdsList) {
        for (Integer phoneId : phoneIdsList.getIds()) {
            System.out.println("-------Received Phone id : " + phoneId);
            JSONObject getPhoneMessage = messageFactory.createGetPhoneConfig(phoneId.toString());
            sendMessage(getPhoneMessage);
        }
        consumeMessages(phoneIdsList.getIds().size());
    }

    @Override
    public void onQueueIdsLoaded(QueueIds queueIds) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onQueueConfigUpdate(QueueConfigUpdate queueConfigUpdate) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onQueueStatusUpdate(QueueStatusUpdate queueStatusUpdate) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onQueueMemberIdsLoaded(QueueMemberIds queueMemberIds) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onQueueMemberConfigUpdate(QueueMemberConfigUpdate queueMemberConfigUpdate) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAgentIdsLoaded(AgentIds agentIds) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAgentConfigUpdate(AgentConfigUpdate agentConfigUpdate) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onQueueMemberRemoved(QueueMemberRemoved queueMemberRemoved) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMeetmeUpdate(MeetmeUpdate meetmeUpdate) {
    }

    @Override
    public void onVoiceMailStatusUpdate(VoiceMailStatusUpdate voiceMailStatusUpdate) {

    }

}
