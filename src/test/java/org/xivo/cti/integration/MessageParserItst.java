package org.xivo.cti.integration;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.MessageDispatcher;
import org.xivo.cti.listener.LoginStepListener;
import org.xivo.cti.message.CtiResponseMessage;
import org.xivo.cti.message.LoginCapasAck;
import org.xivo.cti.message.LoginIdAck;
import org.xivo.cti.message.LoginPassAck;

public class MessageParserItst extends CtiItst implements LoginStepListener {
    private String userId;

    @Before
    public void setUp() throws Exception {
        messageDispatcher = new MessageDispatcher();
        messageDispatcher.addListener(LoginCapasAck.class, this);
        messageDispatcher.addListener(LoginIdAck.class, this);
        messageDispatcher.addListener(LoginPassAck.class, this);
    }

    @Test
    public void test_login_sequence() throws IOException, JSONException, TimeoutException, InterruptedException {
        connectToServer();

        sendLogin(username, "integration-tests");
        LoginIdAck loginIdAck = (LoginIdAck) waitForMessage(LoginIdAck.class.toString());
        messageDispatcher.dispatch(loginIdAck);
        assertNotNull("unable to get login acknowledge", loginIdAck);

        String sessionId = loginIdAck.sesssionId;

        sendLoginPass(password, sessionId);

        LoginPassAck loginPassAck = (LoginPassAck) waitForMessage(LoginPassAck.class.toString());
        messageDispatcher.dispatch(loginPassAck);
        assertNotNull("unable to get login pass acknowledge", loginPassAck);

        int capaId = loginPassAck.capalist.get(0);

        sendLoginCapa(capaId);

        LoginCapasAck loginCapasAck = (LoginCapasAck) waitForMessage(LoginCapasAck.class.toString());
        assertNotNull("unable to get login capas acknowledge", loginCapasAck);
        messageDispatcher.dispatch(loginCapasAck);

        CtiResponseMessage<?> ctiMessage = waitForMessage("");
        messageDispatcher.dispatch(ctiMessage);
        ctiMessage = waitForMessage("");
        messageDispatcher.dispatch(ctiMessage);

        // Thread.sleep(5000);

        // originate("1018", "3000");

        Thread.sleep(5000);

        attendedTransfer("1020");

        Thread.sleep(5000);

        completeTransfer();

        Thread.sleep(5000);

        hangup();

        consumeMessages(40);

        disconnect();
    }

    private void hangup() {
        System.out.println("hangup user :" + this.userId);
        JSONObject message = messageFactory.createHangup();
        sendMessage(message);
    }

    private void attendedTransfer(String destination) {
        System.out.println("attended Transfer : " + destination);
        JSONObject message = messageFactory.createAttendedTransfer(destination);
        sendMessage(message);
    }

    private void completeTransfer() {
        System.out.println("completeTransfer");
        JSONObject message = messageFactory.createCompleteTransfer();
        sendMessage(message);
    }

    private void sendLogin(String username, String identity) throws IOException {
        System.out.println("Login : " + username + " " + identity);
        JSONObject message = messageFactory.createLoginId(username, identity);
        sendMessage(message);
    }

    private void sendLoginPass(String password, String sessionId) throws IOException {
        System.out.println("SendLogin Pass : [" + sessionId + "] " + password);
        JSONObject message = messageFactory.createLoginPass(password, sessionId);
        sendMessage(message);
    }

    private void sendLoginCapa(int capaId) throws IOException {
        JSONObject message = messageFactory.createLoginCapas(capaId);
        sendMessage(message);
    }

    @Override
    public void onLoginCapasAck(LoginCapasAck loginCapasAck) {
        this.userId = loginCapasAck.userId;
        System.out.println("Login Capas User : " + loginCapasAck.userId + " status " + loginCapasAck.presence);
    }

    @Override
    public void onLoginIdAck(LoginIdAck loginIdAck) {
        System.out.println("Login Id Ack received : [" + loginIdAck.sesssionId + " " + loginIdAck.xivoversion);

    }

    @Override
    public void onLoginPassAck(LoginPassAck loginPassAck) {
        System.out.println("Login Pass Ack Recevied : " + loginPassAck.replyId);
    }
}
