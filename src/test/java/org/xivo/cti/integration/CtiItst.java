package org.xivo.cti.integration;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidParameterException;

import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.MessageDispatcher;
import org.xivo.cti.MessageFactory;
import org.xivo.cti.MessageParser;
import org.xivo.cti.listener.LoginStepListener;
import org.xivo.cti.message.CtiResponseMessage;
import org.xivo.cti.message.LoginCapasAck;
import org.xivo.cti.message.LoginIdAck;
import org.xivo.cti.message.LoginPassAck;

public class CtiItst implements LoginStepListener {
    public static final int XIVO_DEFAULT_PORT = 5003;

    protected final String host = "192.168.56.3";
    protected final int port = XIVO_DEFAULT_PORT;
    protected final String username = "bruce";
    protected final String password = "0000";

    protected MessageFactory messageFactory;
    protected MessageDispatcher messageDispatcher;

    private Socket networkConnection = null;
    private BufferedReader inputBuffer = null;
    private final MessageParser messageParser;
    protected String userId = "";

    public CtiItst() {
        messageFactory = new MessageFactory();
        messageParser = new MessageParser();
    }

    protected void sendMessage(JSONObject message) {
        PrintStream output;
        System.out.println(">>> " + message.toString());
        try {
            output = new PrintStream(networkConnection.getOutputStream());
            output.println(message.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected CtiResponseMessage<?> waitForMessage(String className) throws IOException {
        int i = 0;
        while (true) {
            if (i > 10) {
                inputBuffer.close();
                return null;
            }
            i++;
            String line = inputBuffer.readLine();
            System.out.println("<<<[" + i + "] " + line);
            if (line == null) {
                continue;
            }
            CtiResponseMessage<?> ctiMessage = null;
            try {
                ctiMessage = messageParser.parse(new JSONObject(line));
                if (ctiMessage == null) {
                    System.out.println("null message received - Not decoded ?");
                    continue;
                }
                if (className.equals("")) {
                    return ctiMessage;
                }
                if (ctiMessage.getClass().toString().equals(className)) {
                    return ctiMessage;
                }
            } catch (InvalidParameterException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                System.out.println("Unknwon message received : " + e.getMessage());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    protected void connectToServer() {
        try {
            System.out.println("Connecting to " + host + " " + port);
            networkConnection = new Socket(host, port);
        } catch (UnknownHostException e) {
            System.out.println("Unknown host " + host);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("connected .....");
        try {
            inputBuffer = new BufferedReader(new InputStreamReader(networkConnection.getInputStream()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void disconnect() {
        System.out.println("disconnecting");
        inputBuffer = null;
        if (networkConnection != null) {
            try {
                try {
                    networkConnection.shutdownOutput();
                    networkConnection.shutdownInput();
                } catch (IOException e) {
                    System.out.println("Input and output were already closed");
                }
                if (networkConnection != null) {
                    Socket tmp = networkConnection;
                    tmp.close();
                    networkConnection = null;
                }
            } catch (IOException e) {
                System.out.println("Error while cleaning up the network connection");
                e.printStackTrace();
            }
        }
    }

    protected void loginToCtiServer() throws IOException, JSONException {
        connectToServer();

        sendLogin(username, "integration-tests");
        LoginIdAck loginIdAck = (LoginIdAck) waitForMessage(LoginIdAck.class.toString());
        messageDispatcher.dispatch(loginIdAck);
        assertNotNull("unable to get login acknowledge", loginIdAck);

        String sessionId = loginIdAck.sesssionId;

        sendLoginPass(password, sessionId);

        LoginPassAck loginPassAck = (LoginPassAck) waitForMessage(LoginPassAck.class.toString());
        messageDispatcher.dispatch(loginPassAck);
        assertNotNull("unable to get login pass acknowledge", loginPassAck);

        int capaId = loginPassAck.capalist.get(0);

        sendLoginCapa(capaId);

        LoginCapasAck loginCapasAck = (LoginCapasAck) waitForMessage(LoginCapasAck.class.toString());
        assertNotNull("unable to get login capas acknowledge", loginCapasAck);
        messageDispatcher.dispatch(loginCapasAck);

        CtiResponseMessage<?> ctiMessage = waitForMessage("");
        messageDispatcher.dispatch(ctiMessage);
        ctiMessage = waitForMessage("");
        messageDispatcher.dispatch(ctiMessage);

    }

    protected void consumeMessages(int nbOfMessages) {
        for (int i = 0; i < nbOfMessages; i++) {
            CtiResponseMessage<?> ctiMessage;
            try {
                ctiMessage = waitForMessage("");
                System.out.println(ctiMessage);
                messageDispatcher.dispatch(ctiMessage);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendLogin(String username, String identity) throws IOException {
        System.out.println("Login : " + username + " " + identity);
        JSONObject message = messageFactory.createLoginId(username, identity);
        sendMessage(message);
    }

    private void sendLoginPass(String password, String sessionId) throws IOException {
        System.out.println("SendLogin Pass : [" + sessionId + "] " + password);
        JSONObject message = messageFactory.createLoginPass(password, sessionId);
        sendMessage(message);
    }

    private void sendLoginCapa(int capaId) throws IOException {
        JSONObject message = messageFactory.createLoginCapas(capaId);
        sendMessage(message);
    }

    @Override
    public void onLoginCapasAck(LoginCapasAck loginCapasAck) {
        userId = loginCapasAck.userId;
        System.out.println("Login Capas User : " + userId + " status " + loginCapasAck.presence);

    }

    @Override
    public void onLoginIdAck(LoginIdAck loginIdAck) {
        System.out.println("Login Id Ack received : [" + loginIdAck.sesssionId + " " + loginIdAck.xivoversion);

    }

    @Override
    public void onLoginPassAck(LoginPassAck loginPassAck) {
        System.out.println("Login Pass Ack Recevied : " + loginPassAck.replyId);
    }
}
