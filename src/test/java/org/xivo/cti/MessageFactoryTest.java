package org.xivo.cti;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.request.GetConfig;
import org.xivo.cti.model.Endpoint;
import org.xivo.cti.model.QueueStatRequest;

public class MessageFactoryTest {
    private MessageFactory messageFactory;

    @Before
    public void setUp() throws Exception {
        messageFactory = new MessageFactory();
    }

    @Test
    public void class_setup() {
        JSONObject jsonObject = messageFactory.createLoginId("username", "ident");
        assertTrue("class not initialized", jsonObject.has("class"));
        assertTrue("no command id", jsonObject.has("commandid"));
    }

    @Test
    public void createLoginId() throws JSONException {
        JSONObject jsonLoginId = messageFactory.createLoginId("hoghn", "ident-1234");
        assertEquals("invalid class", "login_id", jsonLoginId.get("class"));
        assertEquals("invalid ident", jsonLoginId.get("ident"), "ident-1234");

    }

    @Test
    public void createLoginCapas() throws JSONException {
        JSONObject message = messageFactory.createLoginCapas(3);
        assertEquals("invalid class", message.get("class"), "login_capas");
        assertEquals("invalid loginkind", message.get("loginkind"), "user");
        assertEquals("invalid capaid", message.get("capaid"), 3);
        assertEquals("invalid lastconnwins", message.get("lastconnwins"), false);
        assertEquals("invalid state", message.get("state"), "available");
    }

    @Test
    public void createLoginPass() throws JSONException {
        JSONObject message = messageFactory.createLoginPass("password", "sessionId");
        assertEquals("invalid class", message.get("class"), "login_pass");
    }

    @Test
    public void createLoginCapasWithAgentLogin() throws JSONException {
        JSONObject message = messageFactory.createLoginCapas(3, "2001");
        assertEquals("invalid class", "login_capas", message.get("class"));
        assertEquals("invalid loginkind", message.get("loginkind"), "agent");
        assertEquals("invalid agentPhoneNumber", message.get("agentphonenumber"), "2001");
        assertEquals("invalid capaid", message.get("capaid"), 3);
        assertEquals("invalid lastconnwins", message.get("lastconnwins"), false);
        assertEquals("invalid state", message.get("state"), "available");
    }

    @Test
    public void createGetUserStatus() throws JSONException {
        JSONObject message = messageFactory.createGetUserStatus("78");
        assertNotNull("unable to create get user status message", message);
        assertEquals("invalid user id", "78", message.get("tid"));
    }

    @Test
    public void createGetPhonesList() throws JSONException {
        JSONObject message = messageFactory.createGetPhonesList();
        assertNotNull("unable to create a get phones list message", message);

    }

    @Test
    public void createGetPhoneConfig() throws JSONException {
        JSONObject message = messageFactory.createGetPhoneConfig("32");
        assertNotNull("unable to create get phone config", message);
        assertEquals("invalid phone id", "32", message.get("tid"));
        assertEquals("invalid listname", "phones", message.get("listname"));
    }

    @Test
    public void createGetQueueConfig() throws JSONException {
        JSONObject message = messageFactory.createGetQueueConfig("32");
        assertNotNull("unable to create get queue config", message);
        assertEquals("invalid queue id", "32", message.get("tid"));
        assertEquals("invalid listname", "queues", message.get("listname"));
    }
    @Test
    public void createGetAgentConfig() throws JSONException {
        JSONObject message = messageFactory.createGetAgentConfig("65");
        assertNotNull("unable to create get queue config", message);
        assertEquals("invalid queue id", "65", message.get("tid"));
        assertEquals("invalid listname", "agents", message.get("listname"));
    }

    @Test
    public void createGetPhoneStatus() throws JSONException {
        JSONObject message = messageFactory.createGetPhoneStatus("41");
        assertNotNull("unable to create get phone status", message);
        assertEquals("invalid phone id", "41", message.get("tid"));
        assertEquals("invalid listname", "phones", message.get("listname"));

    }

    @Test
    public void createGetQueueStatus() throws JSONException {
        JSONObject message = messageFactory.createGetQueueStatus("31");
        assertNotNull("unable to create get queue status", message);
        assertEquals("invalid queue id", "31", message.get("tid"));
        assertEquals("invalid listname", "queues", message.get("listname"));

    }

    @Test
    public void createGetUsersList() throws JSONException {
        JSONObject message = messageFactory.createGetUsersList();
        assertNotNull("unable to create a get users list message", message);

    }

    @Test
    public void createGetUserConfig() throws JSONException {
        JSONObject message = messageFactory.createGetUserConfig("56");
        assertNotNull("unable to create a get users config message", message);
        assertEquals("invalid listname", "users", message.get("listname"));
        assertEquals("invalid user id", "56", message.get("tid"));
    }

    @Test
    public void createGetQueueMember() throws JSONException {
        JSONObject message = messageFactory.createGetQueueMemberConfig("Agent/2500,yellow");
        assertNotNull("unable to create a get queue member config message", message);
    }

    @Test
    public void createGetConfig() throws JSONException {
        GetConfig getConfig = new GetConfig("test_function", "test_list");
        JSONObject message = messageFactory.createGetConfig(getConfig);
        assertNotNull("unable to create a get users list message", message);
        assertEquals("invalid class", message.get("class"), "getlist");
        assertEquals("invalid tipbxid", message.get("tipbxid"), "xivo");
        assertEquals("invalid listname", message.get("listname"), "test_list");
        assertEquals("invalid function", message.get("function"), "test_function");
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createDial() throws JSONException {
        Endpoint destination = new Endpoint(Endpoint.EndpointType.EXTEN, "44200");
        JSONObject message = messageFactory.createDial(destination);
        assertNotNull("Unable to create dial message", message);
        assertIpbxCommand(message,"dial");
        assertEquals("invalid destination", "exten:xivo/44200", message.get("destination"));
    }

    @Test
    public void createAttendedTransfer() throws JSONException {
        String destination = "55321";
        JSONObject message = messageFactory.createAttendedTransfer(destination);
        assertNotNull("Unable to create attended transfer message", message);
        assertEquals("invalid class", "attended_transfer", message.get("class"));
        assertEquals("invalid destination", destination, message.get("number"));
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createDirectTransfer() throws JSONException {
        String destination = "77422";
        JSONObject message = messageFactory.createDirectTransfer(destination);
        assertNotNull("Unable to create direct transfer message", message);
        assertEquals("invalid class", "direct_transfer", message.get("class"));
        assertEquals("invalid destination", destination, message.get("number"));
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createCompleteTransfer() throws JSONException {
        JSONObject message = messageFactory.createCompleteTransfer();
        assertNotNull("Unable to create complete transfer message", message);
        assertEquals("invalid class", "complete_transfer", message.get("class"));
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createCancelTransfer() throws JSONException {
        JSONObject message = messageFactory.createCancelTransfer();
        assertNotNull("Unable to create cancel transfer message", message);
        assertEquals("invalid class", "cancel_transfer", message.get("class"));
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createHangUp() throws JSONException {
        JSONObject message = messageFactory.createHangup();
        assertNotNull("Unable to create hangup message", message);
        assertEquals("invalid class", "hangup", message.get("class"));
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createAnswer() throws JSONException {
        JSONObject message = messageFactory.createAnswer();
        assertNotNull("Unable to create answer message", message);
        assertEquals("invalid class", "answer", message.get("class"));
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createOriginate() throws JSONException {
        Endpoint source = new Endpoint(Endpoint.EndpointType.EXTEN, "06789084");
        Endpoint destination = new Endpoint(Endpoint.EndpointType.PHONE, "45");

        JSONObject message = messageFactory.createOriginate(source, destination);
        assertNotNull("Unable to create originate message", message);
        assertIpbxCommand(message,"originate");
        assertEquals("invalid source", "exten:xivo/06789084", message.get("source"));
        assertEquals("invalid destination", "phone:xivo/45", message.get("destination"));

    }

    @Test
    public void createAgentLogin() throws JSONException {
        String phoneNumber = "2002";
        JSONObject message = messageFactory.createAgentLogin(phoneNumber);
        assertNotNull("Unable to create agentLogin message", message);
        assertIpbxCommand(message,"agentlogin");
        assertEquals("invalid agent number", phoneNumber, message.get("agentphonenumber"));
        assertEquals("invalid agent ids", "agent:special:me", message.get("agentids"));
    }

    @Test
    public void createAgentLoginWithAgentId() throws JSONException {
        String phoneNumber = "2002";
        String agentId = "50";
        JSONObject message = messageFactory.createAgentLogin(agentId,phoneNumber);
        assertNotNull("Unable to create agentLogin message", message);
        assertEquals("invalid agent ids", "xivo/50", message.get("agentids"));
    }

    @Test
    public void createAgentLoginWithNoPhoneNumber() throws JSONException {
        String phoneNumber = "";
        String agentId = "50";
        JSONObject message = messageFactory.createAgentLogin(agentId,phoneNumber);
        assertNotNull("Unable to create agentLogin message", message);
        assertEquals("invalid agent ids", "xivo/50", message.get("agentids"));
        assertFalse("should not have agentphonenumber property",message.has("agentphonenumber"));
    }

    @Test
    public void createAgentLogout() throws JSONException {
        String agentId = "4";
        JSONObject message = messageFactory.createAgentLogout(agentId);
        assertNotNull("Unable to create agentLogout message", message);
        assertIpbxCommand(message,"agentlogout");
        assertEquals("invalid agent ids", "xivo/" + agentId, message.get("agentids"));
    }
    @Test
    public void createAgentListen() throws JSONException {
        String agentId = "67";
        JSONObject message = messageFactory.createAgentListen(agentId);
        assertNotNull("Unable to create agent listen message", message);
        assertIpbxCommand(message,"listen");
        assertEquals("invalid destination", "xivo/" + agentId, message.get("destination"));
        assertEquals("invalid subcommand", "start", message.get("subcommand"));
    }

    @Test
    public void createPauseAgent() throws JSONException {
        String agentId = "7";
        JSONObject message = messageFactory.createPauseAgent(agentId);
        assertNotNull("Unable to create pauseagent message", message);
        assertIpbxCommand(message,"queuepause");
        assertEquals("invalid member", "agent:xivo/" + agentId, message.get("member"));
        assertEquals("invalid queue", "queue:xivo/all", message.get("queue"));
    }

    @Test
    public void createUnpauseAgent() throws JSONException {
        String agentId = "89";
        JSONObject message = messageFactory.createUnpauseAgent(agentId);
        assertNotNull("Unable to create pauseagent message", message);
        assertIpbxCommand(message,"queueunpause");
        assertEquals("invalid member", "agent:xivo/" + agentId, message.get("member"));
        assertEquals("invalid queue", "queue:xivo/all", message.get("queue"));
    }

    @Test
    public void createGetAgents() throws JSONException {
        JSONObject message = messageFactory.createGetAgents();
        assertNotNull("unable to create a get agents list message", message);
        assertEquals("invalid class", "getlist", message.get("class"));
        assertEquals("invalid function", "listid", message.get("function"));
        assertEquals("invalid listname", "agents", message.get("listname"));
    }

    @Test
    public void createGetQueues() throws JSONException {
        JSONObject message = messageFactory.createGetQueues();
        assertNotNull("unable to create a get queues list message", message);
        assertEquals("invalid class", "getlist", message.get("class"));
        assertEquals("invalid function", "listid", message.get("function"));
        assertEquals("invalid listname", "queues", message.get("listname"));
    }

    @Test
    public void createGetQueueMembers() throws JSONException {
        JSONObject message = messageFactory.createGetQueueMembers();
        assertNotNull("unable to create a get queue members message", message);
        assertEquals("invalid class", "getlist", message.get("class"));
        assertEquals("invalid function", "listid", message.get("function"));
        assertEquals("invalid listname", "queuemembers", message.get("listname"));
    }

    @Test
    public void createGetAgentsStatus() throws JSONException {
        String agentId = "5";
        JSONObject message = messageFactory.createGetAgentStatus(agentId);
        assertNotNull("unable to create a get agents status message", message);
        assertEquals("invalid class", "getlist", message.get("class"));
        assertEquals("invalid function", "updatestatus", message.get("function"));
        assertEquals("invalid listname", "agents", message.get("listname"));
        assertEquals("invalid user id", agentId.toString(), message.get("tid"));
    }

    @Test
    public void createGetVoiceMailStatus() throws JSONException {
        String voiceMailId = "78";
        JSONObject message = messageFactory.createGetVoicemailStatus(voiceMailId);
        assertNotNull("unable to create a get agents status message", message);
        assertEquals("invalid class", "getlist", message.get("class"));
        assertEquals("invalid function", "updatestatus", message.get("function"));
        assertEquals("invalid listname", "voicemails", message.get("listname"));
        assertEquals("invalid user id", voiceMailId.toString(), message.get("tid"));
    }

    @Test
    public void createUserAvailStatus() throws JSONException {
        String userId = "8";
        String availstate = "away";
        JSONObject message = messageFactory.createUserAvailState(userId, availstate);
        assertNotNull("Unable to create userAvailState message", message);
        assertEquals("invalid class", "availstate", message.get("class"));
        assertEquals("invalid availstate", availstate, message.get("availstate"));
        assertEquals("invalid userId", userId, message.get("userid"));
        assertEquals("invalid ipbxId", "xivo", message.get("ipbxid"));
        assertTrue("no command id", message.has("commandid"));
    }

    @Test
    public void createSearchDirectory() throws JSONException {
        String pattern = "paul";
        JSONObject message = messageFactory.createSearchDirectory(pattern);
        assertNotNull("unable to create search directory message", message);
        assertEquals("invalid class", "directory", message.get("class"));
        assertTrue("no command id", message.has("commandid"));
        assertEquals("no pattern", "paul", message.get("pattern"));

    }

    @Test
    public void createSubscribeToQueueStats() throws JSONException {
        JSONObject message = messageFactory.createSubscribeToQueueStats();
        assertNotNull("unable to create search directory message", message);
        assertTrue("no command id", message.has("commandid"));
        assertEquals("invalid class", "subscribetoqueuesstats", message.get("class"));
    }

    @Test
    public void createGetQueueStatistics() throws JSONException {
        List<QueueStatRequest> qStatRequests = new ArrayList<QueueStatRequest>();
        qStatRequests.add(new QueueStatRequest("2", 3600, 60));

        JSONObject message = messageFactory.createGetQueueStatistics(qStatRequests);
        assertNotNull("unable to create get queue satistics message", message);
        assertEquals("invalid class", "getqueuesstats", message.get("class"));

        JSONObject stats = message.getJSONObject("on");
        assertTrue("unable to encode stat requests", stats.keys().hasNext());
    }

    @Test
    public void createStatRequests() throws JSONException {
        List<QueueStatRequest> qStatRequests = new ArrayList<QueueStatRequest>();
        qStatRequests.add(new QueueStatRequest("22", 3600, 60));
        qStatRequests.add(new QueueStatRequest("37", 1800, 30));
        JSONObject requests = messageFactory.createStatRequests(qStatRequests);
        assertEquals("cannot encode requests ",
                "{\"22\":{\"window\":\"3600\",\"xqos\":\"60\"},\"37\":{\"window\":\"1800\",\"xqos\":\"30\"}}"
                        .replaceAll(" ", ""), requests.toString().replaceAll(" ", ""));
    }

    @Test
    public void createDnd() throws JSONException {
        JSONObject message = messageFactory.createDND(true);
        assertNotNull("unable to create dnd message", message);
        assertTrue("no command id", message.has("commandid"));
        assertEquals("invalid class", "featuresput", message.get("class"));
        assertEquals("invalid function", "enablednd", message.get("function"));
        assertEquals("invalid state", true, message.get("value"));
    }

    @Test
    public void createUnconditionnalForward() throws JSONException {
        JSONObject message = messageFactory.createUnconditionnalForward("45678", true);
        assertNotNull("unable to create unconditionnal forward message", message);
        assertTrue("no command id", message.has("commandid"));
        assertEquals("invalid class", "featuresput", message.get("class"));
        assertEquals("invalid function", "fwd", message.get("function"));
        JSONObject value = message.getJSONObject("value");
        assertEquals("invalid destination", "45678", value.get("destunc"));
        assertEquals("invalid cmd", true, value.get("enableunc"));
    }

    @Test
    public void createNaForward() throws JSONException {
        JSONObject message = messageFactory.createNaForward("74536", true);
        assertNotNull("unable to create forward on no answer message", message);
        assertTrue("no command id", message.has("commandid"));
        assertEquals("invalid class", "featuresput", message.get("class"));
        assertEquals("invalid function", "fwd", message.get("function"));
        JSONObject value = message.getJSONObject("value");
        assertEquals("invalid destination", "74536", value.get("destrna"));
        assertEquals("invalid cmd", true, value.get("enablerna"));
    }

    @Test
    public void createBusyForward() throws JSONException {
        JSONObject message = messageFactory.createBusyForward("98523", true);
        assertNotNull("unable to create forward on busy message", message);
        assertTrue("no command id", message.has("commandid"));
        assertEquals("invalid class", "featuresput", message.get("class"));
        assertEquals("invalid function", "fwd", message.get("function"));
        JSONObject value = message.getJSONObject("value");
        assertEquals("invalid destination", "98523", value.get("destbusy"));
        assertEquals("invalid cmd", true, value.get("enablebusy"));
    }
    @Test
    public void createAddAgentToQueue() throws JSONException {
        String agentId= "34";
        String queueId = "98";
        JSONObject message = messageFactory.createAddAgentToQueue(agentId,queueId);
        assertNotNull("unable to create add agent to queue message",message);
        assertIpbxCommand(message,"queueadd");
        assertEquals("invalid member","agent:xivo/34",message.get("member"));
        assertEquals("invalid queue","queue:xivo/98",message.get("queue"));
    }
    @Test
    public void createRemoveAgentFromQueue() throws JSONException {
        String agentId = "28";
        String queueId = "54";
        JSONObject message = messageFactory.createRemoveAgentFromQueue(agentId, queueId);
        assertNotNull("unable to create remove agent from queue message", message);
        assertIpbxCommand(message, "queueremove");
        assertEquals("invalid member","agent:xivo/28",message.get("member"));
        assertEquals("invalid queue","queue:xivo/54",message.get("queue"));
    }

    @Test
    public void createSubscribeToMeetmeUpdate() throws JSONException {
        JSONObject message = messageFactory.createSubscribeToMeetmeUpdate();
        assertNotNull("unable to create message", message);
        assertEquals("invalid class", "subscribe", message.get("class"));
        assertEquals("invalid class", "meetme_update", message.get("message"));
    }

    @Test
    public void createInviteConferenceRoom() throws JSONException {
        int userid = 5;
        JSONObject message = messageFactory.createInviteConferenceRoom(userid);

        assertNotNull("unable to create remove agent from queue message", message);
        assertEquals("invalid class", message.get("class"), "invite_confroom");
        assertEquals("invalid member","user:xivo/5",message.get("invitee"));
    }

    private void  assertIpbxCommand(JSONObject message, String command) throws JSONException {
        assertTrue("no command id", message.has("commandid"));
        assertEquals("invalid class", "ipbxcommand", message.get("class"));
        assertEquals("invalid command", command, message.get("command"));
    }
}
