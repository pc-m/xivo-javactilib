package org.xivo.cti;

import java.security.InvalidParameterException;
import java.util.Date;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.*;
import org.xivo.cti.message.request.UsersAdded;
import org.xivo.cti.model.AgentStatus;
import org.xivo.cti.model.CallType;
import org.xivo.cti.model.ObjectType;
import org.xivo.cti.model.Payload;
import org.xivo.cti.model.XiVOCall;
import org.xivo.cti.parser.*;

public class MessageParser {

    private static final String DIRECTORY = "directory";
    private static final String FUNCT_LISTID = "listid";
    private static final String FUNCT_UPDATESTATUS = "updatestatus";
    private static final String FUNCT_UPDATECONFIG = "updateconfig";
    private static final String FUNCT_REMOVED_FROM_CONFIG = "delconfig";
    private static final String FUNCT_ADDED_IN_CONFIG = "addconfig";
    private static final String LOGIN_CAPAS = "login_capas";
    private static final String LOGIN_PASS = "login_pass";
    private static final String GETLIST = "getlist";
    private static final String LOGINID = "login_id";
    private static final String HISTORY = "history";
    private static final String SHEET = "sheet";
    private static final String GETQUEUESTATS = "getqueuesstats";
    private static final String IPBX_COMMAND_RESPONSE = "ipbxcommand";
    private static final String MEETME_UPDATE = "meetme_update";

    private final LoginCapasAckParser loginCapasAckParser = new LoginCapasAckParser();
    private final ChannelStatusParser channelStatusParser = new ChannelStatusParser();
    private final DirectoryResultParser directoryResultParser = new DirectoryResultParser();
    private final ListParser listParser = new ListParser();
    private final StatParser statParser = new StatParser();
    private final AddedParser addedParser = new AddedParser();
    private final MeetmeUpdateParser meetmeUpdateParser = new MeetmeUpdateParser();

    private boolean ignoreSheetPayload = false;

    public CtiResponseMessage<?> parseBuffer(String message) throws InvalidParameterException, JSONException {
        return parse(new JSONObject(message));
    }

    public CtiResponseMessage<?> parse(JSONObject jsonObject) throws JSONException, InvalidParameterException {
        if (jsonObject == null)
            throw (new IllegalArgumentException("unable to parse null message"));
        String messageClass = jsonObject.getString("class");
        if (messageClass.equals(LOGINID))
            return parseLoginIdAck(jsonObject);
        else if (messageClass.equals(LOGIN_PASS))
            return parseLoginPassAck(jsonObject);
        else if (messageClass.equals(LOGIN_CAPAS))
            return loginCapasAckParser.parse(jsonObject);
        else if (messageClass.equals(GETLIST))
            return parseGetList(jsonObject);
        else if (messageClass.equals(HISTORY))
            return parseHistoryReply(jsonObject);
        else if (messageClass.equals(SHEET))
            return parseSheet(jsonObject);
        else if (messageClass.equals(DIRECTORY))
            return directoryResultParser.parse(jsonObject);
        else if (messageClass.equals(GETQUEUESTATS))
            return statParser.parse(jsonObject);
        else if (messageClass.equals(IPBX_COMMAND_RESPONSE))
            return parseIpbxCommandResponse(jsonObject);
        else if (messageClass.equals(MEETME_UPDATE))
            return meetmeUpdateParser.parse((jsonObject));
        throw (new IllegalArgumentException("unknown message class"));
    }

    private CtiResponseMessage<?> parseHistoryReply(JSONObject jsonCallHistoryReply) throws JSONException {
        CallHistoryReply callHistoryReply = new CallHistoryReply();
        JSONArray jsonCallHistory = jsonCallHistoryReply.getJSONArray("history");

        for (int i = 0; i < jsonCallHistory.length(); i++) {
            JSONObject jsonCall = jsonCallHistory.getJSONObject(i);
            String callDate = jsonCall.getString("calldate").replace("T", " ");
            callDate = callDate.substring(0, callDate.indexOf('.'));
            long callDuration = Math.round(jsonCall.getDouble("duration"));
            XiVOCall xiVOCall;
            if (jsonCallHistoryReply.getString("mode").equals("0"))
                xiVOCall = new XiVOCall(callDate, callDuration, jsonCall.getString("fullname"), CallType.OUTBOUND);
            else if (jsonCallHistoryReply.getString("mode").equals("1"))
                xiVOCall = new XiVOCall(callDate, callDuration, jsonCall.getString("fullname"), CallType.INBOUND);
            else
                xiVOCall = new XiVOCall(callDate, callDuration, jsonCall.getString("fullname"), CallType.MISSED);
            callHistoryReply.addCall(xiVOCall);
        }

        return callHistoryReply;
    }

    private CtiResponseMessage<?> parseGetList(JSONObject getListJson) throws NumberFormatException, JSONException {
        String function = getListJson.getString("function");
        if (function.equals(FUNCT_UPDATECONFIG))
            return parseConfigUpdate(getListJson);
        if (function.equals(FUNCT_UPDATESTATUS))
            return parseUpdateStatus(getListJson);
        if (function.equals(FUNCT_LISTID))
            return listParser.parse(getListJson);
        if (function.equals(FUNCT_REMOVED_FROM_CONFIG))
            return parseRemoveFromConfig(getListJson);
        if (function.equals(FUNCT_ADDED_IN_CONFIG))
            return addedParser.parse(getListJson);
        return null;
    }

    private CtiResponseMessage<?> parseConfigUpdate(JSONObject jsonConfigUpdate) throws JSONException {
        String listName = jsonConfigUpdate.getString("listname");
        ObjectType objectType = ObjectType.valueOf(listName.toUpperCase());
        switch (objectType) {
        case USERS:
            return parserUserConfigUpdate(jsonConfigUpdate);
        case PHONES:
            return parsePhoneConfigUpdate(jsonConfigUpdate);
        case QUEUES:
            return parseQueueConfigUpdate(jsonConfigUpdate);
        case AGENTS:
            return parseAgentConfigUpdate(jsonConfigUpdate);
        case QUEUEMEMBERS:
            return parseQueueMemberConfigUpdate(jsonConfigUpdate);
        default:
            break;
        }
        return null;
    }

    private CtiResponseMessage<?> parseRemoveFromConfig(JSONObject jsonRemovedFromConfig) throws JSONException {
        String listName = jsonRemovedFromConfig.getString("listname");
        ObjectType objectType = ObjectType.valueOf(listName.toUpperCase());
        switch (objectType) {
        case QUEUEMEMBERS:
            return parseQueueMemberRemoved(jsonRemovedFromConfig);
        default:
            break;
        }
        return null;
    }

    private CtiResponseMessage<?> parseQueueMemberRemoved(JSONObject jsonQmRemoved) throws JSONException {
        QueueMemberRemoved qmRemoved = new QueueMemberRemoved();
        JSONArray members = jsonQmRemoved.getJSONArray("list");
        for (int i = 0; i < members.length(); i++) {
            qmRemoved.setAgentNumber(getAgentNumberFromMember(members.get(i).toString()));
            qmRemoved.setQueueName(getQueueNameFromMember(members.get(i).toString()));
        }

        return qmRemoved;
    }

    private CtiResponseMessage<?> parseAgentConfigUpdate(JSONObject jsonConfigUpdate) throws JSONException {
        AgentConfigUpdate agentConfigUpdate = new AgentConfigUpdate();
        agentConfigUpdate.setId(jsonConfigUpdate.getInt("tid"));
        JSONObject configJson = jsonConfigUpdate.getJSONObject("config");
        agentConfigUpdate.setLastName(configJson.getString("lastname"));
        agentConfigUpdate.setFirstName(configJson.getString("firstname"));
        agentConfigUpdate.setNumber(configJson.getString("number"));
        agentConfigUpdate.setContext(configJson.getString("context"));
        return agentConfigUpdate;
    }

    private CtiResponseMessage<?> parseQueueMemberConfigUpdate(JSONObject jsonConfigUpdate) throws JSONException {
        QueueMemberConfigUpdate queueMemberConfigUpdate = new QueueMemberConfigUpdate();
        String tid = jsonConfigUpdate.getString("tid");
        queueMemberConfigUpdate.setId(tid);
        queueMemberConfigUpdate.setAgentNumber(getAgentNumberFromMember(tid));
        JSONObject configJson = jsonConfigUpdate.getJSONObject("config");
        queueMemberConfigUpdate.setCallsTaken(configJson.getInt("callstaken"));
        queueMemberConfigUpdate.setLastCall(configJson.getString("lastcall"));
        queueMemberConfigUpdate.setPaused(BooleanParser.parse(configJson.getString("paused")));
        queueMemberConfigUpdate.setPenalty(configJson.getInt("penalty"));
        queueMemberConfigUpdate.setQueueName(configJson.getString("queue_name"));
        queueMemberConfigUpdate.setStatus(configJson.getString("status"));
        return queueMemberConfigUpdate;
    }

    private CtiResponseMessage<?> parseQueueConfigUpdate(JSONObject jsonQueueConfigUpdate) throws JSONException {
        QueueConfigUpdate queueConfigUpdate = new QueueConfigUpdate();
        queueConfigUpdate.setId(jsonQueueConfigUpdate.getInt("tid"));
        JSONObject configJson = jsonQueueConfigUpdate.getJSONObject("config");
        queueConfigUpdate.setName(configJson.optString("name", ""));
        queueConfigUpdate.setDisplayName(configJson.optString("displayname", ""));
        queueConfigUpdate.setContext(configJson.optString("context", ""));
        queueConfigUpdate.setNumber(configJson.optString("number", ""));
        return queueConfigUpdate;
    }

    private CtiResponseMessage<?> parsePhoneConfigUpdate(JSONObject jsonPhoneConfigUpdate) throws JSONException {
        PhoneConfigUpdate phoneConfigUpdate = new PhoneConfigUpdate();
        phoneConfigUpdate.setId(jsonPhoneConfigUpdate.getInt("tid"));
        JSONObject phoneConfigJson = jsonPhoneConfigUpdate.getJSONObject("config");

        phoneConfigUpdate.setUserId(Integer.valueOf(phoneConfigJson.optInt("iduserfeatures")));
        phoneConfigUpdate.setNumber(phoneConfigJson.optString("number"));
        phoneConfigUpdate.setContext(phoneConfigJson.optString("context"));

        return phoneConfigUpdate;
    }

    private CtiResponseMessage<?> parseUpdateStatus(JSONObject jsonStatusUpdate) throws JSONException {
        String listName = jsonStatusUpdate.getString("listname");
        ObjectType objectType = ObjectType.valueOf(listName.toUpperCase());
        switch (objectType) {
        case USERS:
            return parseUserUpdateStatus(jsonStatusUpdate);
        case PHONES:
            return parsePhoneStatusUpdate(jsonStatusUpdate);
        case AGENTS:
            return parseAgentStatusUpdate(jsonStatusUpdate);
        case CHANNELS:
            return parseChannelStatusUpdate(jsonStatusUpdate);
        case QUEUES:
            return parseQueueStatusUpdate(jsonStatusUpdate);
        case VOICEMAILS:
            return parseVoiceMailStatusUpdate(jsonStatusUpdate);
        default:
            throw new JSONException("status not decoded for " + listName);
        }
    }
    private CtiResponseMessage<?> parseVoiceMailStatusUpdate(JSONObject statusUpdateMsg) throws JSONException {

        VoiceMailStatusUpdate voiceMailStatusUpdate = new VoiceMailStatusUpdate();
        if (statusUpdateMsg.has("tid")) {
            voiceMailStatusUpdate.setVoiceMailId(statusUpdateMsg.optLong("tid", 0));
        }
        JSONObject status = statusUpdateMsg.getJSONObject("status");
        voiceMailStatusUpdate.setNewMessages(status.optInt("new", 0));
        voiceMailStatusUpdate.setWaitingMessages(status.optInt("waiting",0));
        voiceMailStatusUpdate.setOldMessages(status.optInt("old",0));

        return voiceMailStatusUpdate;
    }

    private CtiResponseMessage<?> parseQueueStatusUpdate(JSONObject statusUpdateMsg) throws JSONException {
        QueueStatusUpdate statusUpdate = new QueueStatusUpdate();
        JSONObject status = statusUpdateMsg.getJSONObject("status");
        JSONArray agentMembers = status.getJSONArray("agentmembers");
        for (int i = 0; i < agentMembers.length(); i++) {
            statusUpdate.addAgentMember(Integer.valueOf((String) agentMembers.get(i)));
        }
        JSONArray phoneMembers = status.getJSONArray("phonemembers");
        for (int i = 0; i < phoneMembers.length(); i++) {
            statusUpdate.addPhoneMember(Integer.valueOf((String) phoneMembers.get(i)));
        }

        return statusUpdate;
    }

    private CtiResponseMessage<?> parsePhoneStatusUpdate(JSONObject jsonPhoneStatusUpdate) throws JSONException {
        PhoneStatusUpdate phoneStatusUpdate = new PhoneStatusUpdate();
        phoneStatusUpdate.setLineId(Integer.valueOf(jsonPhoneStatusUpdate.getInt("tid")));
        JSONObject jsonStatus = jsonPhoneStatusUpdate.getJSONObject("status");
        phoneStatusUpdate.setHintStatus(jsonStatus.optString("hintstatus"));
        if (jsonStatus.has("channels") && jsonStatus.getJSONArray("channels").length() != 0
                && phoneStatusUpdate.getHintStatus().equals("0")) {
            throw new IllegalArgumentException("phone status idle with channels");
        }
        return phoneStatusUpdate;
    }

    private CtiResponseMessage<?> parseAgentStatusUpdate(JSONObject jsonAgentStatusUpdate) throws JSONException {
        int agentId = (Integer.valueOf(jsonAgentStatusUpdate.getInt("tid")));
        JSONObject jsonStatus = jsonAgentStatusUpdate.getJSONObject("status");
        AgentStatus status = AgentStatusParser.parseStatus(jsonStatus);
        AgentStatusUpdate agentStatusUpdate = new AgentStatusUpdate(agentId, status);

        return agentStatusUpdate;
    }

    private CtiResponseMessage<?> parseChannelStatusUpdate(JSONObject jsonChannelStatusUpdate) throws JSONException {
        return channelStatusParser.parseStatusUpdate(jsonChannelStatusUpdate);
    }

    private CtiResponseMessage<?> parseUserUpdateStatus(JSONObject userStatusUpdateJson) throws NumberFormatException,
            JSONException {
        UserStatusUpdate userStatusUpdate = new UserStatusUpdate();
        userStatusUpdate.setUserId(Integer.valueOf(userStatusUpdateJson.getString("tid")));
        JSONObject statusJson = userStatusUpdateJson.getJSONObject("status");
        userStatusUpdate.setStatus(statusJson.getString("availstate"));
        return userStatusUpdate;
    }

    private CtiResponseMessage<?> parserUserConfigUpdate(JSONObject userConfigUpdateJson) throws NumberFormatException,
            JSONException {
        UserConfigUpdate userConfigUpdate = new UserConfigUpdate();
        userConfigUpdate.setUserId(Integer.valueOf(userConfigUpdateJson.getString("tid")));

        JSONObject userConfigJson = userConfigUpdateJson.getJSONObject("config");
        if (userConfigJson.has("enablednd")) {
            userConfigUpdate.setDndEnabled(BooleanParser.parse(userConfigJson.getString("enablednd")));
        }
        if (userConfigJson.has("enablerna")) {
            userConfigUpdate.setNaFwdEnabled(BooleanParser.parse(userConfigJson.getString("enablerna")));
        }
        if (userConfigJson.has("destrna")) {
            userConfigUpdate.setNaFwdDestination(userConfigJson.getString("destrna"));
        }
        if (userConfigJson.has("agentid")) {
            userConfigUpdate.setAgentId(userConfigJson.optInt("agentid", 0));
        }
        if (userConfigJson.has("enableunc")) {
            userConfigUpdate.setUncFwdEnabled(BooleanParser.parse(userConfigJson.getString("enableunc")));
        }
        if (userConfigJson.has("destunc")) {
            userConfigUpdate.setUncFwdDestination(userConfigJson.getString("destunc"));
        }
        if (userConfigJson.has("enablebusy")) {
            userConfigUpdate.setBusyFwdEnabled(BooleanParser.parse(userConfigJson.getString("enablebusy")));
        }
        if (userConfigJson.has("destbusy")) {
            userConfigUpdate.setBusyDestination(userConfigJson.getString("destbusy"));
        }
        if (userConfigJson.has("firstname")) {
            userConfigUpdate.setFirstName(userConfigJson.getString("firstname"));
        }
        if (userConfigJson.has("lastname")) {
            userConfigUpdate.setLastName(userConfigJson.getString("lastname"));
        }
        if (userConfigJson.has("fullname")) {
            userConfigUpdate.setFullName(userConfigJson.getString("fullname"));
        }
        if (userConfigJson.has("mobilephonenumber")) {
            userConfigUpdate.setMobileNumber(userConfigJson.getString("mobilephonenumber"));
        }
        if (userConfigJson.has("linelist")) {
            JSONArray jsonLines = userConfigJson.getJSONArray("linelist");
            for (int i = 0; i < jsonLines.length(); i++) {
                userConfigUpdate.addLineId(Integer.valueOf((String) jsonLines.get(i)));
            }
        }
        if (userConfigJson.has("enablevoicemail")) {
            userConfigUpdate.setVoiceMailEnabled(BooleanParser.parse(userConfigJson.getString("enablevoicemail")));
        }
        if (userConfigJson.has("voicemailid")) {
            userConfigUpdate.setVoiceMailId(userConfigJson.optLong("voicemailid", 0));
        }
        return userConfigUpdate;
    }

    private CtiResponseMessage<?> parseLoginIdAck(JSONObject loginIdAckJson) throws JSONException {
        LoginIdAck loginIdAck = new LoginIdAck();
        loginIdAck.sesssionId = loginIdAckJson.getString("sessionid");
        loginIdAck.timenow = loginIdAckJson.getDouble("timenow");
        loginIdAck.xivoversion = loginIdAckJson.getString("xivoversion");
        return loginIdAck;
    }

    private CtiResponseMessage<?> parseLoginPassAck(JSONObject loginPassAckJson) throws JSONException {
        LoginPassAck loginPassAck = new LoginPassAck();
        JSONArray capas = loginPassAckJson.getJSONArray("capalist");
        for (int i = 0; i < capas.length(); i++) {
            loginPassAck.capalist.add((Integer) capas.get(i));
        }
        loginPassAck.timenow = loginPassAckJson.getDouble("timenow");
        loginPassAck.replyId = loginPassAckJson.getLong("replyid");
        return loginPassAck;
    }

    public void ignoreSheetPlayload() {
        ignoreSheetPayload = true;
    }

    public Sheet parseSheet(JSONObject sheetJson) throws JSONException, InvalidParameterException {
        Sheet sheet = new Sheet();
        sheet.channel = sheetJson.getString("channel");
        sheet.compressed = sheetJson.getBoolean("compressed");
        if (!ignoreSheetPayload) {
            sheet.payload = new Payload(sheetJson.getString("payload"), sheet.compressed);
        }
        sheet.serial = sheetJson.getString("serial");
        sheet.timenow = new Date(sheetJson.getLong("timenow"));
        return sheet;
    }

    public IpbxCommandResponse parseIpbxCommandResponse(JSONObject ipbxCmdRespJson) throws JSONException,
            InvalidParameterException {

        String error_string = ipbxCmdRespJson.getString("error_string");
        Date timenow = new Date(ipbxCmdRespJson.getLong("timenow"));
        IpbxCommandResponse ipbxCmdResp = new IpbxCommandResponse(error_string, timenow);
        return ipbxCmdResp;
    }

    public static String getAgentNumberFromMember(String queueMember) {
        return (queueMember.split(",")[0]).split("/")[1];
    }
    public static String getQueueNameFromMember(String queueMember) {
        return queueMember.split(",")[1];
    }
}
