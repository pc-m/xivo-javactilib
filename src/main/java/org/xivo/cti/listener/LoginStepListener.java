package org.xivo.cti.listener;

import org.xivo.cti.message.LoginCapasAck;
import org.xivo.cti.message.LoginIdAck;
import org.xivo.cti.message.LoginPassAck;

public interface LoginStepListener {

    void onLoginCapasAck(LoginCapasAck loginCapasAck);

    void onLoginIdAck(LoginIdAck loginIdAck);

    void onLoginPassAck(LoginPassAck loginPassAck);
}
