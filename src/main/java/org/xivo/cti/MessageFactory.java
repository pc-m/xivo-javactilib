package org.xivo.cti;

import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.request.*;
import org.xivo.cti.model.Endpoint;
import org.xivo.cti.model.ObjectType;
import org.xivo.cti.model.QueueStatRequest;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageFactory {
    private static String KEY_COMMANDID = "commandid";

    private static long commandId = 1;

    private Logger logger = Logger.getLogger(getClass().getName());

    public JSONObject createLoginId(String username, String identity) {
        LoginId loginId = new LoginId(username, identity);
        JSONObject jsonLoginId = createCtiRequest(loginId);
        try {
            jsonLoginId.accumulate("company", loginId.getCompany());
            jsonLoginId.accumulate("ident", loginId.getIdent());
            jsonLoginId.accumulate("userlogin", loginId.getUserlogin());
            jsonLoginId.accumulate("version", loginId.getVersion());
            jsonLoginId.accumulate("xivoversion", loginId.getXivoversion());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the loginId message" + jsonLoginId.toString(), e);
        }

        return jsonLoginId;
    }

    public JSONObject createLoginPass(String password, String sessionId) {
        LoginPass loginPass = new LoginPass(password, sessionId);
        JSONObject jsonLoginPass = createCtiRequest(loginPass);

        try {
            jsonLoginPass.accumulate("hashedpassword", loginPass.getHashedpassword());

        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the loginPass message" + jsonLoginPass.toString(), e);
        }
        return jsonLoginPass;
    }

    public JSONObject createLoginCapas(int capaId) {
        LoginCapas loginCapas = prepareLoginCapas(capaId);
        return convertLoginCapasToJson(loginCapas);
    }

    public JSONObject createLoginCapas(int capaId, String agentNo) {
        LoginCapas loginCapas = prepareLoginCapasWithAgentLogin(capaId, agentNo);
        return convertLoginCapasToJson(loginCapas);
    }

    private JSONObject convertLoginCapasToJson(LoginCapas loginCapas) {
        JSONObject jsonLoginCapas = createCtiRequest(loginCapas);

        try {
            jsonLoginCapas.accumulate("capaid", loginCapas.getCapaid());
            jsonLoginCapas.accumulate("loginkind", loginCapas.getLoginkind());
            jsonLoginCapas.accumulate("state", loginCapas.getState());
            jsonLoginCapas.accumulate("lastconnwins", loginCapas.isLastconnwins());
            if (loginCapas.getAgentNumber() != null)
                jsonLoginCapas.accumulate("agentphonenumber", loginCapas.getAgentNumber());

        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the loginCapas message" + jsonLoginCapas.toString(), e);
        }

        return jsonLoginCapas;
    }

    private LoginCapas prepareLoginCapas(int capaId) {
        return new LoginCapas(capaId);
    }

    private LoginCapas prepareLoginCapasWithAgentLogin(int capaId, String agentNumber) {
        LoginCapas loginCapas = new LoginCapas(capaId, agentNumber);
        return loginCapas;
    }

    public JSONObject createGetUserConfig(String userId) {
        return createGetObjectConfig(ObjectType.USERS, userId);
    }

    public JSONObject createGetQueueMemberConfig(String memberId) {
        return createGetObjectConfig(ObjectType.QUEUEMEMBERS, memberId);
    }

    public JSONObject createGetPhoneConfig(String phoneId) {
        return createGetObjectConfig(ObjectType.PHONES, phoneId);
    }

    public JSONObject createGetQueueConfig(String queueId) {
        return createGetObjectConfig(ObjectType.QUEUES, queueId);
    }
    public JSONObject createGetAgentConfig(String agentId) {
        return createGetObjectConfig(ObjectType.AGENTS, agentId);
    }

    protected JSONObject createGetObjectConfig(ObjectType objectType, String objectId) {
        GetObjectConfig getConfig = new GetObjectConfig(objectType, objectId);
        JSONObject jsonGetConfig = createGetConfig(getConfig);

        try {
            jsonGetConfig.accumulate("tid", objectId);
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the getConfig message" + jsonGetConfig.toString(), e);
        }
        return jsonGetConfig;

    }

    public JSONObject createGetUserStatus(String userId) {
        return createStatus(new GetObjectStatus(ObjectType.USERS, userId));
    }

    public JSONObject createGetPhoneStatus(String lineId) {
        return createStatus(new GetObjectStatus(ObjectType.PHONES, lineId));
    }

    public JSONObject createGetQueueStatus(String queueId) {
        return createStatus(new GetObjectStatus(ObjectType.QUEUES, queueId));
    }

    public JSONObject createGetAgentStatus(String agentId) {
        return createStatus(new GetObjectStatus(ObjectType.AGENTS, agentId));
    }

    public JSONObject createGetVoicemailStatus(String voiceMailId) {
        return createStatus(new GetObjectStatus(ObjectType.VOICEMAILS, voiceMailId));
    }

    private JSONObject createStatus(GetObjectStatus getObjectStatus) {
        JSONObject jsonGetPhoneStatus = createGetConfig(getObjectStatus);
        try {
            jsonGetPhoneStatus.accumulate("tid", getObjectStatus.getObjectId().toString());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the GetPhoneStatus message" + jsonGetPhoneStatus.toString(), e);
        }
        return jsonGetPhoneStatus;
    }

    protected JSONObject createGetConfig(GetConfig getConfig) {
        JSONObject jsonGetConfig = createCtiRequest(getConfig);
        try {
            jsonGetConfig.accumulate("tipbxid", getConfig.getTipBxid());
            jsonGetConfig.accumulate("listname", getConfig.getListName());
            jsonGetConfig.accumulate("function", getConfig.getFunction());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the GetConfig message" + jsonGetConfig.toString(), e);
        }
        return jsonGetConfig;
    }

    public JSONObject createDial(Endpoint destination) {
        Dial dial = new Dial(destination);
        JSONObject jsonDial = createIpbxCommand(dial);
        try {
            jsonDial.accumulate("destination", dial.getDestination().getDestinationUrl());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the Dial message" + jsonDial.toString(), e);
        }
        return jsonDial;
    }

    public JSONObject createAttendedTransfer(String destination) {
        return createTransfer(new AttendedTransfer(destination));
    }

    public JSONObject createDirectTransfer(String destination) {
        return createTransfer(new DirectTransfer(destination));
    }

    public JSONObject createCompleteTransfer() {
        return createCtiRequest(new CompleteTransfer());
    }

    public JSONObject createCancelTransfer() {
        return createCtiRequest(new CancelTransfer());
    }

    public JSONObject createHangup() {
        return createCtiRequest(new Hangup());
    }

    public JSONObject createAnswer() {
        return createCtiRequest(new Answer());
    }

    public JSONObject createOriginate(Endpoint source, Endpoint destination) {
        Originate originate = new Originate(source, destination);
        JSONObject jsonOriginate = createIpbxCommand(originate);
        try {
            jsonOriginate.accumulate("source", originate.getSource().getDestinationUrl());
            jsonOriginate.accumulate("destination", originate.getDestination().getDestinationUrl());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the Originate message" + jsonOriginate.toString(), e);
        }
        return jsonOriginate;
    }

    public JSONObject createAgentLogin(String agentId, String phoneNumber) {
        return createAgentLogin(new AgentLogin(agentId,phoneNumber));
    }

    public JSONObject createAgentLogin(String phoneNumber) {
        return createAgentLogin(new AgentLogin(phoneNumber));
    }

    private JSONObject createAgentLogin(AgentLogin agentLogin) {
        JSONObject jsonAgentLogin = createIpbxCommand(agentLogin);
        try {
            if (!agentLogin.getPhoneNumber().equals(""))
                jsonAgentLogin.accumulate("agentphonenumber", agentLogin.getPhoneNumber());
            jsonAgentLogin.accumulate("agentids", agentLogin.getAgentIds());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the AgentLogin message" + jsonAgentLogin.toString(), e);
        }
        return jsonAgentLogin;
    }

    public JSONObject createAgentLogout(String agentId) {
        AgentLogout agentLogout = new AgentLogout(agentId);
        JSONObject jsonAgentLogout = createIpbxCommand(agentLogout);
        try {
            jsonAgentLogout.accumulate("agentids", agentLogout.getAgentIds());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the AgentLogout message" + jsonAgentLogout.toString(), e);
        }
        return jsonAgentLogout;
    }
    //  {"class": "ipbxcommand", "command": "listen", "commandid": 363963032, "destination": "xivo/19", "subcommand": "start"}
    public JSONObject createAgentListen(String agentId) {
        AgentListen agentListen = new AgentListen(agentId);
        JSONObject jsonAgentListen = createIpbxCommand(agentListen);
        try {
            jsonAgentListen.accumulate("destination", agentListen.getAgentIds());
            jsonAgentListen.accumulate("subcommand", "start");
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the Agent listen message" + jsonAgentListen.toString(), e);
        }
        return jsonAgentListen;
    }

    public JSONObject createPauseAgent(String agentId) {
        return createPauseUnpauseAgent(new PauseAgent(agentId));
    }

    public JSONObject createUnpauseAgent(String agentId) {
        return createPauseUnpauseAgent(new UnpauseAgent(agentId));
    }

    public JSONObject createGetUsersList() {
        return createGetConfig(new GetUsers());
    }

    public JSONObject createGetPhonesList() {
        return createGetConfig(new GetPhones());
    }

    public JSONObject createGetQueues() {
        return createGetConfig(new GetQueues());
    }

    public JSONObject createGetAgents() {
        return createGetConfig(new GetAgents());
    }

    public JSONObject createGetQueueMembers() {
        return createGetConfig(new GetQueueMembers());
    }

    public JSONObject createUserAvailState(String userId, String availState) {
        UserAvailState userAvailState = new UserAvailState(userId, availState);
        JSONObject jsonUserAvailState = createCtiRequest(userAvailState);
        try {
            jsonUserAvailState.accumulate("availstate", userAvailState.getAvailstate());
            jsonUserAvailState.accumulate("userid", userAvailState.getUserId());
            jsonUserAvailState.accumulate("ipbxid", userAvailState.getIpbxid());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the UserAvailState message" + jsonUserAvailState.toString(), e);
        }
        return jsonUserAvailState;
    }

    public JSONObject createSearchDirectory(String pattern) {
        SearchDirectory searchDirectory = new SearchDirectory(pattern);
        JSONObject jsonSearchDirectory = createCtiRequest(searchDirectory);
        try {
            jsonSearchDirectory.accumulate("pattern", searchDirectory.getPattern());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the SearchDirectory message" + jsonSearchDirectory.toString(), e);
        }
        return jsonSearchDirectory;
    }

    public JSONObject createAddAgentToQueue(String agentId, String queueId) {
        AddAgentToQueue atq = new AddAgentToQueue(agentId,queueId);
        JSONObject message = createIpbxCommand(atq);
        try {
            message.put("member",atq.getMember());
            message.put("queue",atq.getQueue());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the AddAgentToQueue message" + message.toString(), e);
        }
        return message;
    }
    public JSONObject createRemoveAgentFromQueue(String agentId, String queueId) {
        RemoveAgentFromQueue rmfq = new RemoveAgentFromQueue(agentId,queueId);
        JSONObject message = createIpbxCommand(rmfq);
        try {
            message.put("member",rmfq.getMember());
            message.put("queue",rmfq.getQueue());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the RemoveAgentFromQueue message" + message.toString(), e);
        }
        return message;
    }

    public JSONObject createDND(boolean state) {
        Dnd msg = new Dnd(state);
        JSONObject jsonMsg = createCtiRequest(msg);
        try {
            jsonMsg.put("function", msg.getFunction());
            jsonMsg.put("value", msg.getState());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the DND message" + jsonMsg.toString(), e);
        }
        return jsonMsg;
    }

    public JSONObject createUnconditionnalForward(String destination, boolean enabled) {
        UnconditionnalFwd msg = new UnconditionnalFwd(destination, enabled);
        return createForward(msg);
    }

    public JSONObject createNaForward(String destination, boolean enabled) {
        NaFwd msg = new NaFwd(destination, enabled);
        return createForward(msg);
    }

    public JSONObject createBusyForward(String destination, boolean enabled) {
        BusyFwd msg = new BusyFwd(destination, enabled);
        return createForward(msg);
    }

    private JSONObject createForward(Fwd msg) {
        JSONObject jsonMsg = createCtiRequest(msg);
        try {
            jsonMsg.put("function", msg.getFunction());
            JSONObject value = new JSONObject();
            value.put("dest" + msg.getType(), msg.getDestination());
            value.put("enable" + msg.getType(), msg.isEnabled());
            jsonMsg.put("value", value);
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the Forward message" + jsonMsg.toString(), e);
        }
        return jsonMsg;
    }

    public JSONObject createSubscribeToQueueStats() {
        return createCtiRequest(new SubscribeToQueueStats());
    }

    public JSONObject createGetQueueStatistics(List<QueueStatRequest> qStatRequests) {
        GetQueueStatistics getQueueStatistics = new GetQueueStatistics(qStatRequests);
        JSONObject jsonMsg = createCtiRequest(getQueueStatistics);
        try {
            JSONObject jsonRequests = createStatRequests(getQueueStatistics.getRequests());
            jsonMsg.put("on", jsonRequests);
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling GetQueueStatistics the Forward message" + jsonMsg.toString(), e);
        }
        return jsonMsg;
    }

    public JSONObject createSubscribeToMeetmeUpdate() throws JSONException {
        JSONObject res = createCtiRequest(new SubscribeToMeetmeUpdate());
        res.accumulate("message", "meetme_update");
        return res;
    }

    protected JSONObject createStatRequests(List<QueueStatRequest> requests) throws JSONException {
        JSONObject jsonRequests = new JSONObject();
        for (QueueStatRequest qsr : requests) {
            JSONObject statParams = new JSONObject();
            statParams.put("window", Integer.toString(qsr.getWindow()));
            statParams.put("xqos", Integer.toString(qsr.getXqos()));
            jsonRequests.put(qsr.getQueueId(), statParams);
        }
        return jsonRequests;
    }

    private JSONObject createTransfer(Transfer transfer) {
        JSONObject jsonMsg = createCtiRequest(transfer);
        try {
            jsonMsg.accumulate("number", transfer.getDestination());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the Transfer message" + jsonMsg.toString(), e);
        }
        return jsonMsg;
    }

    private JSONObject createPauseUnpauseAgent(PauseUnPauseAgent pupag) {
        JSONObject jsonPupag = createIpbxCommand(pupag);
        try {
            jsonPupag.accumulate("member", pupag.getMember());
            jsonPupag.accumulate("queue", pupag.getQueue());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the PauseUnPauseAgent message" + jsonPupag.toString(), e);
        }
        return jsonPupag;
    }

    private JSONObject createIpbxCommand(IpbxCommand command) {
        JSONObject jsonObject = createCtiRequest(command);
        try {
            jsonObject.accumulate("command", command.getCommand());
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error filling the IpbxCommand message" + jsonObject.toString(), e);
        }
        return jsonObject;
    }

    public JSONObject createInviteConferenceRoom(int userid) throws JSONException {
        JSONObject jsonObject = createCtiRequest(new InviteConferenceRoom(userid));
        jsonObject.accumulate("invitee", "user:xivo/" + userid);
        return jsonObject;
    }

    private JSONObject createCtiRequest(CtiRequest request) {
        JSONObject message = new JSONObject();
        try {
            message.accumulate("class", request.getClaz());
            message.accumulate(KEY_COMMANDID, commandId++);
        } catch (JSONException e) {
            logger.log(Level.WARNING, "Error creating the base CtiRequest", e);
        }
        return message;
    }
}
