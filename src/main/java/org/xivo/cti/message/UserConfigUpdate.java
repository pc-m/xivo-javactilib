package org.xivo.cti.message;

import java.util.ArrayList;
import java.util.List;

import org.xivo.cti.listener.ConfigListener;

public class UserConfigUpdate extends CtiResponseMessage<ConfigListener> {
    private int userId;
    private boolean dndEnabled;
    private boolean naFwdEnabled;
    private String naFwdDestination = null;
    private boolean uncFwdEnabled;
    private String uncFwdDestination = null;
    private boolean busyFwdEnabled;
    private String busyFwdDestination = null;
    private String firstName;
    private String lastName;
    private String fullName;
    private String mobileNumber;
    private int agentId = 0;
    private final List<Integer> lineIds;
    private long voiceMailId = 0;

    private boolean voiceMailEnabled;

    public UserConfigUpdate() {
        lineIds = new ArrayList<Integer>();
    }
    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public void setDndEnabled(boolean dndEnabled) {
        this.dndEnabled = dndEnabled;
    }

    public boolean isDndEnabled() {
        return dndEnabled;
    }

    @Override
    public void notify(ConfigListener listener) {
        listener.onUserConfigUpdate(this);

    }

    public void setNaFwdEnabled(boolean naFwdEnabled) {
        this.naFwdEnabled = naFwdEnabled;
    }

    public boolean isNaFwdEnabled() {
        return naFwdEnabled;
    }

    public String getNaFwdDestination() {
        return naFwdDestination;
    }

    public void setNaFwdDestination(String naFwdDestination) {
        this.naFwdDestination = naFwdDestination;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isUncFwdEnabled() {
        return uncFwdEnabled;
    }

    public void setUncFwdEnabled(boolean uncFwdEnabled) {
        this.uncFwdEnabled = uncFwdEnabled;
    }

    public String getUncFwdDestination() {
        return uncFwdDestination;
    }

    public void setUncFwdDestination(String destination) {
        uncFwdDestination = destination;
    }

    public boolean isBusyFwdEnabled() {
        return busyFwdEnabled;
    }

    public void setBusyFwdEnabled(boolean busyEnabled) {
        this.busyFwdEnabled = busyEnabled;
    }

    public String getBusyFwdDestination() {
        return busyFwdDestination;
    }

    public void setBusyDestination(String busyDestination) {
        this.busyFwdDestination = busyDestination;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public int getAgentId() {
        return agentId;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }


    public List<Integer> getLineIds() {
        return lineIds;
    }
    public void addLineId(Integer id) {
        lineIds.add(id);
    }

    public boolean isVoiceMailEnabled() {
        return voiceMailEnabled;
    }

    public void setVoiceMailEnabled(boolean voiceMailEnabled) {
        this.voiceMailEnabled = voiceMailEnabled;
    }

    public long getVoiceMailId() {
        return voiceMailId;
    }

    public void setVoiceMailId(long voiceMailId) {
        this.voiceMailId = voiceMailId;
    }
}
