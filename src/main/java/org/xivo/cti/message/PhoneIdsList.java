package org.xivo.cti.message;

import org.xivo.cti.listener.IdsListener;


public class PhoneIdsList extends IdsList {

    @Override
    public void notify(IdsListener listener) {
        listener.onPhoneIdsLoaded(this);

    }
}
