package org.xivo.cti.message.request;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginPass extends CtiRequest {
    private final String hashedpassword;

    private Logger logger = Logger.getLogger(getClass().getName());

    private static String bytes2String(byte[] bytes) {
        StringBuilder string = new StringBuilder();
        for (byte b : bytes) {
            String hexString = Integer.toHexString(0x00FF & b);
            string.append(hexString.length() == 1 ? "0" + hexString : hexString);
        }
        return string.toString();
    }

    public LoginPass(String password, String sessionId) {
        super("login_pass");
        MessageDigest sha1;
        byte[] sDigest = null;
        try {
            sha1 = MessageDigest.getInstance("SHA1");
            sDigest = sha1.digest((sessionId + ":" + password).getBytes());
        } catch (NoSuchAlgorithmException e) {
            logger.log(Level.WARNING, "Could not encode the password", e);
        }
        hashedpassword = bytes2String(sDigest);

    }

    public String getHashedpassword() {
        return hashedpassword;
    }

}
