package org.xivo.cti.message.request;

public class SubscribeToQueueStats extends CtiRequest {

    public SubscribeToQueueStats() {
        super("subscribetoqueuesstats");
    }

}
