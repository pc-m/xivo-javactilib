package org.xivo.cti.message.request;

import java.util.List;

import org.xivo.cti.model.QueueStatRequest;

public class GetQueueStatistics extends CtiRequest {
    private List<QueueStatRequest> requests;

    public GetQueueStatistics(List<QueueStatRequest> qStatRequests) {
        super("getqueuesstats");
        requests = qStatRequests;
    }

    public List<QueueStatRequest> getRequests() {
        return requests;
    }
    

}
