package org.xivo.cti.message.request;

import org.xivo.cti.model.ObjectType;

public class GetStatusOrConfig  extends GetConfig {

    private final String objectId;

    public GetStatusOrConfig(String function,ObjectType objectType, String objectId) {
        super(function,objectType.toString());
        this.objectId = objectId;
    }

    public String getObjectId() {
        return objectId;
    }

}
