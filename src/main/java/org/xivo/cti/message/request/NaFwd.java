package org.xivo.cti.message.request;

public class NaFwd extends Fwd {

    public NaFwd(String destination, boolean enabled) {
        super(destination,enabled,fwdType.rna);
    }
}
