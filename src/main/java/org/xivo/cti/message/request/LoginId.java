package org.xivo.cti.message.request;

public class LoginId extends CtiRequest {
    private final String company = "default";
    private final String ident;
    private final String userlogin;
    private final String version = "9999";
    private final String xivoversion = "1.2";

    public LoginId(String username, String identity) {
        super("login_id");
        userlogin = username;
        ident = identity;
    }

    public String getCompany() {
        return company;
    }

    public String getIdent() {
        return ident;
    }

    public String getUserlogin() {
        return userlogin;
    }

    public String getVersion() {
        return version;
    }

    public String getXivoversion() {
        return xivoversion;
    }

}
