package org.xivo.cti.message.request;

public class UserAvailState extends CtiRequest {
    private final String ipbxid = "xivo";
    private final String availstate;
    private final String userid;

    public UserAvailState(String userid, String availstate) {
        super("availstate");
        this.userid = userid;
        this.availstate = availstate;
    }

    public String getAvailstate() {
        return availstate;
    }

    public String getIpbxid() {
        return ipbxid;
    }

    public String getUserId() {
        return userid;
    }

}
