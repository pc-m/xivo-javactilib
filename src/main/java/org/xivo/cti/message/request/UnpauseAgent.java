package org.xivo.cti.message.request;

public class UnpauseAgent extends PauseUnPauseAgent {

    public UnpauseAgent(String agentId) {
        super("queueunpause", agentId);
    }
}
