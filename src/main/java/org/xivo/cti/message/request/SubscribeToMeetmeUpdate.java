package org.xivo.cti.message.request;

public class SubscribeToMeetmeUpdate extends CtiRequest {

    public SubscribeToMeetmeUpdate() {
        super("subscribe");
    }
}
