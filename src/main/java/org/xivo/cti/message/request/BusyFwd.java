package org.xivo.cti.message.request;

public class BusyFwd extends Fwd {

    public BusyFwd(String destination, boolean enabled) {
        super(destination,enabled,fwdType.busy);
    }
}
