package org.xivo.cti.message.request;

/**
 * Created by jylebleu on 26/08/14.
 */
public class RemoveAgentFromQueue extends AgentQueueConfig {

    public RemoveAgentFromQueue(String agentId, String queueId) {
        super(QUEUEREMOVE,agentId,queueId);
    }
}
