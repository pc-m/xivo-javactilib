package org.xivo.cti.message;

import java.util.ArrayList;
import java.util.List;

import org.xivo.cti.listener.LoginStepListener;

public class LoginPassAck extends CtiResponseMessage<LoginStepListener>{

	public List<Integer> capalist = new ArrayList<Integer>();
	public double timenow;
	public long replyId;


    @Override
    public void notify(LoginStepListener listener) {
        listener.onLoginPassAck(this);

    }

}
