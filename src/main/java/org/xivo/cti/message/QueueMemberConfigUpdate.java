package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public class QueueMemberConfigUpdate extends CtiResponseMessage<ConfigListener> {
    private String id;
    private int callsTaken;
    private String lastCall;
    private boolean paused;
    private int penalty;
    private String queueName;
    private String status;
    private String agentNumber;

    public QueueMemberConfigUpdate() {
        callsTaken = 0;
        lastCall = "";
        paused = false;
        penalty = 0;
        queueName = "";
    }

    @Override
    public void notify(ConfigListener listener) {
        listener.onQueueMemberConfigUpdate(this);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCallsTaken() {
        return callsTaken;
    }

    public void setCallsTaken(int callsTaken) {
        this.callsTaken = callsTaken;
    }

    public String getLastCall() {
        return lastCall;
    }

    public void setLastCall(String lastCall) {
        this.lastCall = lastCall;
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

}
