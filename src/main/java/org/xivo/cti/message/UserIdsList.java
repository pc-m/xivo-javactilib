package org.xivo.cti.message;

import org.xivo.cti.listener.IdsListener;

public class UserIdsList extends IdsList {

    @Override
    public void notify(IdsListener listener) {
        listener.onUserIdsLoaded(this);
    }

}
