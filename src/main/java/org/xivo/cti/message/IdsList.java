package org.xivo.cti.message;

import java.util.ArrayList;
import java.util.List;

import org.xivo.cti.listener.IdsListener;

public abstract class IdsList extends CtiResponseMessage<IdsListener> {

    List<Integer> ids;

    public IdsList() {
        ids = new ArrayList<Integer>();
    }

    public List<Integer> getIds() {
        return ids;
    }

    public void add(Integer id) {
        ids.add(id);
    }
}
