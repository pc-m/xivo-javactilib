package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public class UserStatusUpdate extends CtiResponseMessage<ConfigListener>{
    int userId;
    String status;

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public void notify(ConfigListener listener) {
       listener.onUserStatusUpdate(this);

    }

}
