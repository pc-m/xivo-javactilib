package org.xivo.cti.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.model.AgentStatus;
import org.xivo.cti.model.Availability;
import org.xivo.cti.model.StatusReason;

public class AgentStatusParser {

    public static AgentStatus parseStatus(JSONObject jsonStatus) throws IllegalArgumentException {
        try {
            String phonenumber = jsonStatus.getString("phonenumber");
            String availability = jsonStatus.getString("availability");
            JSONArray queues = jsonStatus.optJSONArray("queues");
            StatusReason reason = extractReason(jsonStatus);
            AgentStatus agentStatus = new AgentStatus(phonenumber, Availability.valueOf(availability.toUpperCase()),
                    reason);
            agentStatus.setSince(DateParser.parse(jsonStatus.optDouble("availability_since")));
            agentStatus.setQueues(extractQueue(queues));
            return agentStatus;
        } catch (Exception e) {
            throw new IllegalArgumentException("JSON parsing problem", e);
        }
    }

    private static StatusReason extractReason(JSONObject jsonStatus) throws JSONException {
        if (jsonStatus.has("on_call_acd") && jsonStatus.getBoolean("on_call_acd"))
            return StatusReason.ON_CALL_ACD;
        else if (jsonStatus.has("on_call_nonacd") && jsonStatus.getBoolean("on_call_nonacd"))
            return StatusReason.ON_CALL_NONACD;
        else if (jsonStatus.has("on_wrapup") && jsonStatus.getBoolean("on_wrapup"))
            return StatusReason.ON_WRAPUP;
        else
            return StatusReason.NONE;
    }

    private static List<Integer> extractQueue(JSONArray queues) throws NumberFormatException, JSONException {
        List<Integer> queueIds = new ArrayList<Integer>();
        if (queues == null) return queueIds;

        for (int i = 0; i < queues.length(); i++) {
            queueIds.add(Integer.valueOf((String) queues.get(i)));
        }
        return queueIds;
    }
}
