package org.xivo.cti.model;

public class Counter {
    private StatName statName;
    private int value;

    public Counter(StatName statName, int value) {
        this.statName = statName;
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        Counter counter = (Counter) obj;
        return (counter.statName.equals(this.statName) && counter.value == this.value);
    }

    public StatName getStatName() {
        return statName;
    }

    public int getValue() {
        return value;
    };

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return "{ \"statname\": " + statName + " , \"value\": " + value + "}";
    }
}
