package org.xivo.cti.model;

import java.util.Date;
import java.util.List;

public class Meetme {

    private String number;
    private String name;
    private boolean pinRequired;
    private Date startTime;
    private List<MeetmeMember> members;

    public Meetme(String number, String name, boolean pinRequired, Date startTime, List<MeetmeMember> members) {
        this.number = number;
        this.name = name;
        this.pinRequired = pinRequired;
        this.startTime = startTime;
        this.members = members;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public boolean isPinRequired() {
        return pinRequired;
    }

    public Date getStartTime() {
        return startTime;
    }

    public List<MeetmeMember> getMembers() { return members; }
}
