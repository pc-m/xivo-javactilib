package org.xivo.cti.model;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.InvalidParameterException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.xivo.cti.model.sheet.Profile;

public class Payload {
		
	private Profile profile = null;
	
	public Payload(String payload, boolean compressed) throws InvalidParameterException {
		byte[] xml;

		try {
			xml = decodeFromNetworkFormat(payload, compressed);
		
		} catch (UnsupportedEncodingException e) {
			InvalidParameterException invalidParams = new InvalidParameterException("Unsupported payload encoding");
			invalidParams.initCause(e);
			throw invalidParams;
		
		} catch (DataFormatException e) {
			InvalidParameterException invalidParams = new InvalidParameterException("Invalid xml data payload format");
			invalidParams.initCause(e);
			throw invalidParams;
		}
		
		this.profile = unmarshall(xml);				
	}	

	public Profile getProfile() {
		return profile;
	}
	
	public String toString() {
	    if (profile != null)
	        return profile.toString();
	    else
	        return "";
	}

	private byte[] decodeFromNetworkFormat(String base64Payload, boolean compressed)
			throws DataFormatException, UnsupportedEncodingException 
	{
		byte[] decodedPayload = DatatypeConverter.parseBase64Binary(base64Payload);
		if (compressed)
			return decodeFromZLIB(decodedPayload);
		else
			throw new DataFormatException("Uncompressed payload not implemented");
	}

	private byte[] decodeFromZLIB(byte[] compressedPayload) 
			throws DataFormatException, UnsupportedEncodingException
	{
		int xmlLength = extraitLength(compressedPayload);		
		
		byte[] result = new byte[xmlLength];
		Inflater decompressor = new Inflater();
		
		decompressor.setInput(compressedPayload, 4, compressedPayload.length - 4);
		decompressor.inflate(result);
		decompressor.end();
		return result;
	}
	
	private int extraitLength(byte[] compressedPayload)
	{
		final ByteBuffer bb = ByteBuffer.wrap(compressedPayload);
		bb.order(ByteOrder.BIG_ENDIAN);
		return bb.getInt();
	}
	
	private Profile unmarshall(byte[] xml) throws InvalidParameterException {
		Profile profile = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance("org.xivo.cti.model.sheet");		
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			profile = (Profile) unmarshaller.unmarshal(new ByteArrayInputStream(xml));
		} catch (JAXBException e) {
			InvalidParameterException invalidParameter = new InvalidParameterException("Invalid XML in payload input");
			invalidParameter.initCause(e);
			throw invalidParameter;
		}
		this.profile = profile;
		return this.profile;		
	}
	
}
